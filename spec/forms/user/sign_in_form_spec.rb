# frozen_string_literal: true

RSpec.describe User::SignInForm do
  subject(:sign_in_form) { described_class.new(params) }

  describe 'Validations' do
    let(:params) do
      {
        email: 'p_smith@example.com',
        password: 'Qwerty12345!!!'
      }
    end

    it 'is valid with correct attributes' do
      expect(sign_in_form).to be_valid
    end

    context 'with email' do
      it_behaves_like 'validates presence', :email

      it 'is not valid with an incorrect email format' do
        sign_in_form.email = 'test!test.com'

        expect(sign_in_form.validate).to be_falsy
      end
    end

    context 'with password' do
      it_behaves_like 'validates presence', :password
    end
  end

  describe '#authenticate' do
    let!(:user) do
      create(
        :user,
        email: 'hello@example.com',
        password: 'Qwerty12345!!!',
        password_confirmation: 'Qwerty12345!!!'
      )
    end

    context 'with valid params' do
      let(:params) { { email: 'hello@example.com', password: 'Qwerty12345!!!' } }

      it 'returns the specific user' do
        expect(sign_in_form.authenticate).to eq(user)
      end
    end

    context 'with invalid params' do
      let(:params) { { email: '', password: 'Qwerty12345!!!' } }

      it 'returns false' do
        expect(sign_in_form.authenticate).to be_falsey
      end
    end
  end
end
