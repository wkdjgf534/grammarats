# frozen_string_literal: true

RSpec.describe User::RemindPasswordForm do
  subject(:remind_password_form) { described_class.new(params) }

  let(:user) { create(:user, email: 'p_smith@example.com') }
  let(:params) do
    {
      email: 'p_smith@example.com'
    }
  end

  before { params }

  describe 'Validations' do
    it 'is valid with correct attributes' do
      user
      expect(remind_password_form).to be_valid
    end

    context 'with email' do
      it_behaves_like 'validates presence', :email

      context 'when user exists' do
        it 'returns true' do
          user
          expect(remind_password_form.validate).to be_truthy
        end
      end

      context 'when user does not exist' do
        it 'returns false' do
          remind_password_form.email = 'p_smith1@example.com'

          expect(remind_password_form.validate).to be_falsy
        end
      end

      it 'is not valid with an incorrect email format' do
        remind_password_form.email = 'test!test.com'

        expect(remind_password_form.validate).to be_falsy
      end
    end
  end

  describe '#user' do
    it 'returns user' do
      user
      expect(remind_password_form.user).to eq(user)
    end
  end
end
