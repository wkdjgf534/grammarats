# frozen_string_literal: true

RSpec.describe User::PasswordForm do
  subject(:password_form) { described_class.new(params) }

  describe 'Validations' do
    let(:params) do
      {
        password_challenge: 'Qwerty12345!',
        password: 'Qwerty12345!!!',
        password_confirmation: 'Qwerty12345!!!'
      }
    end

    it 'is valid with correct attributes' do
      expect(password_form).to be_valid
    end

    context 'with password_challenge' do
      it_behaves_like 'validates presence', :password_challenge
    end

    context 'with password' do
      it_behaves_like 'validates min length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:min] - 1)
      it_behaves_like 'validates max length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:max] + 1)
    end

    context 'with password_confirmation' do
      it_behaves_like 'validates presence', :password_confirmation

      it 'is not valid without a password confirmation' do
        password_form.password_confirmation = nil
        expect(password_form).not_to be_valid
      end
    end

    context 'when new password is not identical to password_confirmation' do
      context 'when password' do
        it 'returns an error' do
          password_form.password = password_form.password_challenge

          expect(password_form.validate).to be_falsey
        end
      end

      context 'when password and password_confirmation' do
        it 'returns an error' do
          password_form.password = password_form.password_challenge
          password_form.password_confirmation = password_form.password_challenge

          expect(password_form.validate).to be_falsey
        end
      end
    end
  end
end
