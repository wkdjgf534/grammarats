# frozen_string_literal: true

RSpec.describe User::ResetPasswordForm do
  subject(:reset_password_form) { described_class.new(params) }

  let(:user) { create(:user) }

  describe 'Validations' do
    let(:params) do
      {
        password: 'Qwerty12345!!!',
        password_confirmation: 'Qwerty12345!!!'
      }
    end

    it 'is valid with correct attributes' do
      expect(reset_password_form).to be_valid
    end

    context 'with password' do
      it_behaves_like 'validates min length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:min] - 1)
      it_behaves_like 'validates max length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:max] + 1)
    end

    context 'with password_confirmation' do
      it_behaves_like 'validates presence', :password_confirmation

      it 'is not valid without a password confirmation' do
        reset_password_form.password_confirmation = nil

        expect(reset_password_form).not_to be_valid
      end
    end
  end

  describe '#user' do
    let(:params) { { token: user.generate_token_for(:password_reset) } }

    it 'returns user' do
      expect(reset_password_form.user).to eq(user)
    end
  end
end
