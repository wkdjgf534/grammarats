# frozen_string_literal: true

RSpec.describe User::RegistrationForm do
  subject(:registration_form) { described_class.new(params) }

  describe 'Validations' do
    let(:params) do
      {
        email: 'p_smith@example.com',
        password: 'Qwerty12345!!!',
        password_confirmation: 'Qwerty12345!!!',
        terms_of_service: '1'
      }
    end

    it 'is valid with correct attributes' do
      expect(registration_form).to be_valid
    end

    context 'with terms of service' do
      it 'is not valid with a terms_of_service equals to 0' do
        registration_form.terms_of_service = '0'

        expect(registration_form).not_to be_valid
      end
    end

    context 'with email' do
      it_behaves_like 'validates presence', :email

      it 'is not valid, must be unique email' do
        create(:user, email: 'p_smith@example.com')

        expect(registration_form.validate).to be_falsy
      end

      it 'is not valid with an incorrect email format' do
        registration_form.email = 'test!test.com'

        expect(registration_form.validate).to be_falsy
      end
    end

    context 'with password' do
      it_behaves_like 'validates min length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:min] - 1)
      it_behaves_like 'validates max length',
                      :password,
                      Faker::Lorem.characters(number: User::PASSWORD[:max] + 1)
    end

    context 'with password_confirmation' do
      it_behaves_like 'validates presence', :password_confirmation

      it 'is not valid without a password confirmation' do
        registration_form.password_confirmation = nil

        expect(registration_form).not_to be_valid
      end
    end
  end

  describe '#save' do
    context 'with valid params' do
      let(:params) do
        {
          email: 'hello@example.com',
          password: 'Qwerty12345!!!',
          password_confirmation: 'Qwerty12345!!!',
          terms_of_service: '1'
        }
      end

      it 'creates a new user' do
        expect { registration_form.save }.to change(User, :count).by(1)
      end
    end

    context 'with invalid params' do
      let(:params) do
        {
          email: '',
          password: 'Qwerty12345!!!',
          password_confirmation: 'Qwerty12345!!!',
          terms_of_service: '1'
        }
      end

      it 'does not create a new user' do
        expect { registration_form.save }.not_to change(User, :count)
      end
    end
  end
end
