# frozen_string_literal: true

RSpec.describe Decks::DeleteService, type: :service do
  subject(:delete_service) { described_class.new(deck:) }

  let(:deck) { create(:deck, :archived, user:) }
  let(:user) { create(:user) }

  before { deck }

  describe '#call' do
    context 'when successful' do
      it 'removes a record from DB' do
        expect { delete_service.call }.to change(Deck, :count).by(-1)
      end

      it 'returns a positive response' do
        expect(delete_service.call).to eq(Dry::Monads::Success())
      end
    end

    context 'when failed' do
      let(:deck) { nil }

      it 'does not remove a record from DB' do
        expect { delete_service.call }.not_to change(Deck, :count)
      end

      it 'returns a negative response' do
        expect(delete_service.call).to eq(Dry::Monads::Failure(I18n.t('errors.object_is_nil')))
      end
    end
  end
end
