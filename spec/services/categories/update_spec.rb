# frozen_string_literal: true

=begin
RSpec.describe Categories::UpdateService, type: :service do
  subject(:update_service) { described_class.call() }

  let(:cupdate) { create(:category, :archived) }

  before { category }

  describe '#call' do
    context 'when successful' do
      it 'updated a record' do
        expect { update_service.call }.to change(Category, :count).by(-1)
      end

      it 'returns a positive response' do
        expect(delete_service.call).to eq(Dry::Monads::Success())
      end
    end

    context 'when failed' do
      let(:category) { nil }

      it 'does not update a record' do
        expect { update_service.call }.not_to change(Category, :count)
      end

      it 'returns a negative response' do
        expect(delete_service.call).to eq(Dry::Monads::Failure(I18n.t('errors.object_is_nil')))
      end
    end
  end
end
=end
