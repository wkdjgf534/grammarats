# frozen_string_literal: true

RSpec.describe Courses::Validator do
  subject(:validator) { described_class.call(params) }

  describe '.call' do
    context 'when nil' do
      let(:category) { nil }
      let(:params) { { category: } }

      it 'returns a negative response' do
        expect(validator).to eq(Dry::Monads::Failure(I18n.t('errors.object_is_nil')))
      end
    end

    context 'when activated' do
    end
  end
end
