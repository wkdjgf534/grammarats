# frozen_string_literal: true

RSpec.describe Courses::DeleteService, type: :service do
  subject(:delete_service) { described_class.new(course:) }

  let(:course) { create(:course, :archived, user:) }
  let(:user) { create(:user) }

  before { course }

  describe '#call' do
    context 'when successful' do
      it 'removes a record from DB' do
        expect { delete_service.call }.to change(Course, :count).by(-1)
      end

      it 'returns a positive response' do
        expect(delete_service.call).to eq(Dry::Monads::Success())
      end
    end

    context 'when failed' do
      let(:course) { nil }

      it 'does not remove a record from DB' do
        expect { delete_service.call }.not_to change(Course, :count)
      end

      it 'returns a negative response' do
        expect(delete_service.call).to eq(Dry::Monads::Failure(I18n.t('errors.object_is_nil')))
      end
    end
  end
end
