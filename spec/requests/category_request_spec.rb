# frozen_string_literal: true

RSpec.describe 'Category' do
  describe 'GET #index' do
    let(:categories) { create_list(:category, 3) }

    before do
      categories
      get categories_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns category titles' do
      category_titles = Category.pluck(:title)
      expect(response.body).to include(*category_titles)
    end
  end

  describe 'GET #show' do
    let(:category) { create(:category) }

    before { get category_path(category) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns category title' do
      expect(response.body).to include(category.title)
    end
  end
end
