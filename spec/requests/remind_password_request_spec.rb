# frozen_string_literal: true

RSpec.describe 'Remind Password' do
  let(:user) { create(:user) }

  describe 'GET #new' do
    before { get new_remind_password_path }

    it 'renders a successful response' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    before { user }

    context 'with valid attributes' do
      let(:valid_form_params) { { email: user.email } }

      it 'returns status found' do
        post remind_password_path, params: { user_remind_password_form: valid_form_params }
        expect(response).to have_http_status(:found)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_form_params) { { email: '' } }

      it 'returns status unprocessable_entity' do
        post remind_password_path, params: { user_remind_password_form: invalid_form_params }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
