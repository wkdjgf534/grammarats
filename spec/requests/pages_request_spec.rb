# frozen_string_literal: true

RSpec.describe 'Page' do
  describe 'GET #main' do
    before { get main_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #about_us' do
    before { get about_us_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #terms_of_service' do
    before { get terms_of_service_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #services' do
    before { get services_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end
end
