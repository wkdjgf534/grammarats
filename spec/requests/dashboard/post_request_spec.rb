# frozen_string_literal: true

RSpec.describe 'Dashboard::Document' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:category) { create(:category) }
  let(:article) { create(:post, user:, document:, category:) }

  before { login(user) }

  describe 'GET #index' do
    before do
      article
      get dashboard_posts_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns post title' do
      expect(response.body).to include(article.title)
    end
  end

  describe 'GET #show' do
    let(:get_post) do
      get dashboard_post_path(article)
    end

    it 'returns status ok' do
      get_post
      expect(response).to have_http_status(:ok)
    end

    it 'returns post title' do
      get_post
      expect(response.body).to include(article.title)
    end
  end

  describe 'GET #edit' do
    it 'returns status ok' do
      get edit_dashboard_post_path(article)
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET #new' do
    before { get new_dashboard_post_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_post) do
        post dashboard_posts_path,
             params: { post: attributes_for(:post).merge(category_id: category.id, document_id: document.id) }
      end

      it 'creates a new Post' do
        expect { create_post }.to change(Post, :count).by(1)
      end

      it 'returns status found' do
        create_post
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard post list' do
        create_post
        expect(response).to redirect_to(dashboard_posts_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_post) { post dashboard_posts_path, params: { post: attributes_for(:invalid_post) } }

      it 'does not create a new Post' do
        expect { create_post }.not_to change(Post, :count)
      end

      it 'returns status unprocessable_entity' do
        create_post
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      let(:update_post) do
        put dashboard_post_path(article), params: { post: { title: '  article title  ' } }
      end

      it 'saves changes in the database' do
        update_post
        expect(article.reload).to have_attributes({ title: 'Article title', slug: 'article-title' })
      end

      it 'returns status found' do
        update_post
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard post list' do
        update_post
        expect(response).to redirect_to(dashboard_posts_path)
      end
    end

    context 'with invalid attributes' do
      before { put dashboard_post_path(article), params: { post: attributes_for(:invalid_post) } }

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when successful' do
      before { article }

      let(:removes_post) { delete dashboard_post_path(article) }

      it 'removes the existed Post' do
        expect { removes_post }.to change(Post, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_post
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard post list' do
        removes_post
        expect(response).to redirect_to(dashboard_posts_path)
      end
    end
  end
end
