# frozen_string_literal: true

RSpec.describe 'Dashboard::Paragraph' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:paragraph) { create(:paragraph, document:) }

  before { login(user) }

  describe 'GET #new' do
    before { get new_dashboard_document_paragraph_path(document) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_paragraph) { post dashboard_document_paragraphs_path(document), params: { paragraph: attributes_for(:paragraph) } }

      it 'creates a new Paragraph' do
        expect { create_paragraph }.to change(Paragraph, :count).by(1)
      end

      it 'returns status found' do
        create_paragraph
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard deck' do
        create_paragraph
        expect(response).to redirect_to(dashboard_document_path(document))
      end
    end

    context 'with invalid attributes' do
      let(:create_paragraph) { post dashboard_document_paragraphs_path(document), params: { paragraph: attributes_for(:invalid_paragraph) } }

      it 'does not create a new Paragraph' do
        expect { create_paragraph }.not_to change(Paragraph, :count)
      end

      it 'returns status unprocessable_entity' do
        create_paragraph
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #edit' do
    before { get edit_dashboard_paragraph_path(paragraph) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before { put dashboard_paragraph_path(paragraph), params: { paragraph: { title: '  new paragraph  ' } } }

      it 'saves changes in the database' do
        expect(paragraph.reload).to have_attributes({ title: 'New paragraph' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard document' do
        expect(response).to redirect_to(dashboard_document_path(document))
      end
    end

    context 'with invalid attributes' do
      before do
        put dashboard_paragraph_path(paragraph), params: { paragraph: attributes_for(:invalid_paragraph) }
      end

      it 'does not save changes in the database' do
        expect(paragraph.reload).to have_attributes({ title: paragraph.title })
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

    describe 'DELETE #destroy' do
    before { paragraph }

    context 'when successful' do
      let(:removes_paragraph) { delete dashboard_paragraph_path(paragraph) }

      it 'removes the existed Document' do
        expect { removes_paragraph }.to change(Paragraph, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_paragraph
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard deck' do
        removes_paragraph
        expect(response).to redirect_to(dashboard_document_path(document))
      end
    end
  end
end
