# frozen_string_literal: true

RSpec.describe 'Dashboard::Home' do
  describe 'GET #main' do
    before { get dashboard_root_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:found)
    end
  end
end
