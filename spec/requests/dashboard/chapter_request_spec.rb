# frozen_string_literal: true

RSpec.describe 'Dashboard::Chapter' do
  let(:user) { create(:user) }
  let(:course) { create(:course, user:) }
  let(:chapter) { create(:chapter, course:) }

  before { login(user) }

  describe 'GET #new' do
    before { get new_dashboard_course_chapter_path(course) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_chapter) { post dashboard_course_chapters_path(course), params: { chapter: attributes_for(:chapter) } }

      it 'creates a new Card' do
        expect { create_chapter }.to change(Chapter, :count).by(1)
      end

      it 'returns status found' do
        create_chapter
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard deck' do
        create_chapter
        expect(response).to redirect_to(dashboard_course_path(course))
      end
    end

    context 'with invalid attributes' do
      let(:create_chapter) { post dashboard_course_chapters_path(course), params: { chapter: attributes_for(:invalid_chapter) } }

      it 'does not create a new Card' do
        expect { create_chapter }.not_to change(Chapter, :count)
      end

      it 'returns status unprocessable_entity' do
        create_chapter
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #edit' do
    before { get edit_dashboard_chapter_path(chapter) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before { put dashboard_chapter_path(chapter), params: { chapter: { title: '  new chapter  ' } } }

      it 'saves changes in the database' do
        expect(chapter.reload).to have_attributes({ title: 'New chapter' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard course' do
        expect(response).to redirect_to(dashboard_course_path(course))
      end
    end

    context 'with invalid attributes' do
      before do
        put dashboard_chapter_path(chapter), params: { chapter: attributes_for(:invalid_chapter) }
      end

      it 'does not save changes in the database' do
        expect(chapter.reload).to have_attributes({ title: chapter.title })
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

    describe 'DELETE #destroy' do
    before { chapter }

    context 'when successful' do
      let(:removes_chapter) { delete dashboard_chapter_path(chapter) }

      it 'removes the existed Document' do
        expect { removes_chapter }.to change(Chapter, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_chapter
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard course' do
        removes_chapter
        expect(response).to redirect_to(dashboard_course_path(course))
      end
    end
  end
end
