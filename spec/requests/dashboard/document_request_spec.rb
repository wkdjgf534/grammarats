# frozen_string_literal: true

RSpec.describe 'Dashboard::Document' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }

  before { login(user) }

  describe 'GET #index' do
    let(:documents) { create_list(:document, 3, user:) }

    before do
      documents
      get dashboard_documents_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns document titles' do
      documents_titles = documents.pluck(:title)
      expect(response.body).to include(*documents_titles)
    end
  end

  describe 'GET #new' do
    before { get new_dashboard_document_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_document) do
        post dashboard_documents_path,
             params: { document: attributes_for(:document) }
      end

      it 'creates a new Document' do
        expect { create_document }.to change(Document, :count).by(1)
      end

      it 'returns status found' do
        create_document
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard documents list' do
        create_document
        expect(response).to redirect_to(dashboard_documents_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_document) do
        post dashboard_documents_path,
             params: { document: attributes_for(:invalid_document) }
      end

      it 'does not create a new Document Type' do
        expect { create_document }.not_to change(Document, :count)
      end

      it 'returns status unprocessable_entity' do
        create_document
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put dashboard_document_path(document),
            params: { document: { title: '  english language  ', description: '  description  ' } }
      end

      it 'saves changes in the database' do
        expect(document.reload).to have_attributes(
          { title: 'English language', description: 'Description' }
        )
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin document list' do
        expect(response).to redirect_to(dashboard_documents_path)
      end
    end

    context 'with invalid attributes' do
      it 'returns status unprocessable_entity' do
        put dashboard_document_path(document), params: { document: attributes_for(:invalid_document) }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { document }

    context 'when successful' do
      let(:removes_document) { delete dashboard_document_path(document) }

      it 'removes the existed Document' do
        expect { removes_document }.to change(Document, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_document
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard documents list' do
        removes_document
        expect(response).to redirect_to(dashboard_documents_path)
      end
    end
  end
end
