# frozen_string_literal: true

RSpec.describe 'Dashboard::Course' do
  let(:course) { create(:course, user:) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    let(:courses) { create_list(:course, 3) }

    before do
      course
      get dashboard_courses_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns course titles' do
      course_titles = Course.pluck(:title)
      expect(response.body).to include(*course_titles)
    end
  end

  describe 'GET #new' do
    before { get new_dashboard_course_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_course) { post dashboard_courses_path, params: { course: attributes_for(:course) } }

      it 'creates a new Course' do
        expect { create_course }.to change(Course, :count).by(1)
      end

      it 'returns status found' do
        create_course
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard course list' do
        create_course
        expect(response).to redirect_to(dashboard_courses_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_course) { post dashboard_courses_path, params: { course: attributes_for(:invalid_course) } }

      it 'does not create a new Course' do
        expect { create_course }.not_to change(Course, :count)
      end

      it 'returns status unprocessable_entity' do
        create_course
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #show' do
    before { get dashboard_course_path(course) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns course titles' do
      expect(response.body).to include(course.title)
    end
  end

  describe 'GET #edit' do
    before { get edit_dashboard_course_path(course) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put dashboard_course_path(course),
            params: { course: { title: '  new course  ', description: '  new description  ' } }
      end

      it 'saves changes in the database' do
        expect(course.reload).to have_attributes(
          { title: 'New course', description: 'New description' }
        )
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard course list' do
        expect(response).to redirect_to(dashboard_courses_path)
      end
    end

    context 'with invalid attributes' do
      before do
        put dashboard_course_path(course), params: { course: attributes_for(:invalid_course) }
      end

      it 'does not save changes in the database' do
        expect(course.reload).to have_attributes(
          { title: course.title, description: course.description }
        )
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { course }

    context 'when successful' do
      let(:removes_course) { delete dashboard_course_path(course) }

      it 'removes the existed Document' do
        expect { removes_course }.to change(Course, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_course
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard course list' do
        removes_course
        expect(response).to redirect_to(dashboard_courses_path)
      end
    end
  end
end
