# frozen_string_literal: true

RSpec.describe 'Dashboard::Card' do
  let(:user) { create(:user) }
  let(:deck) { create(:deck, user:) }
  let(:card) { create(:card, deck:) }

  before { login(user) }

  describe 'GET #new' do
    before { get new_dashboard_deck_card_path(deck) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_card) { post dashboard_deck_cards_path(deck), params: { card: attributes_for(:card) } }

      it 'creates a new Card' do
        expect { create_card }.to change(Card, :count).by(1)
      end

      it 'returns status found' do
        create_card
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard deck' do
        create_card
        expect(response).to redirect_to(dashboard_deck_path(deck))
      end
    end

    context 'with invalid attributes' do
      let(:create_card) { post dashboard_deck_cards_path(deck), params: { card: attributes_for(:invalid_card) } }

      it 'does not create a new Card' do
        expect { create_card }.not_to change(Card, :count)
      end

      it 'returns status unprocessable_entity' do
        create_card
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #edit' do
    before { get edit_dashboard_card_path(card) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before { put dashboard_card_path(card), params: { card: { title: '  new card  ' } } }

      it 'saves changes in the database' do
        expect(card.reload).to have_attributes({ title: 'New card' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard deck' do
        expect(response).to redirect_to(dashboard_deck_path(deck))
      end
    end

    context 'with invalid attributes' do
      before do
        put dashboard_card_path(card), params: { card: attributes_for(:invalid_card) }
      end

      it 'does not save changes in the database' do
        expect(card.reload).to have_attributes({ title: card.title })
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { card }

    context 'when successful' do
      let(:removes_card) { delete dashboard_card_path(card) }

      it 'removes the existed Document' do
        expect { removes_card }.to change(Card, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_card
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard deck' do
        removes_card
        expect(response).to redirect_to(dashboard_deck_path(deck))
      end
    end
  end
end
