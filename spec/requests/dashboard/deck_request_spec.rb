# frozen_string_literal: true

RSpec.describe 'Dashboard::Deck' do
  let(:deck) { create(:deck, user:) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    let(:decks) { create_list(:deck, 3, user:) }

    before do
      decks
      get dashboard_decks_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns deck titles' do
      deck_titles = Deck.pluck(:title)
      expect(response.body).to include(*deck_titles)
    end
  end

  describe 'GET #new' do
    before { get new_dashboard_deck_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_deck) { post dashboard_decks_path, params: { deck: attributes_for(:deck) } }

      it 'creates a new Deck' do
        expect { create_deck }.to change(Deck, :count).by(1)
      end

      it 'returns status found' do
        create_deck
        expect(response).to have_http_status(:found)
      end

      it 'redirect to dashboard deck list' do
        create_deck
        expect(response).to redirect_to(dashboard_decks_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_deck) { post dashboard_decks_path, params: { deck: attributes_for(:invalid_deck) } }

      it 'does not create a new Deck' do
        expect { create_deck }.not_to change(Deck, :count)
      end

      it 'returns status unprocessable_entity' do
        create_deck
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #show' do
    before { get dashboard_deck_path(deck) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns deck titles' do
      expect(response.body).to include(deck.title)
    end
  end

  describe 'GET #edit' do
    before { get edit_dashboard_deck_path(deck) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put dashboard_deck_path(deck),
            params: { deck: { title: '  new deck  ', description: '  new description  ' } }
      end

      it 'saves changes in the database' do
        expect(deck.reload).to have_attributes(
          { title: 'New deck', description: 'New description' }
        )
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to dashboard deck list' do
        expect(response).to redirect_to(dashboard_decks_path)
      end
    end

    context 'with invalid attributes' do
      before do
        put dashboard_deck_path(deck), params: { deck: attributes_for(:invalid_deck) }
      end

      it 'does not save changes in the database' do
        expect(deck.reload).to have_attributes(
          { title: deck.title, description: deck.description }
        )
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { deck }

    context 'when successful' do
      let(:removes_deck) { delete dashboard_deck_path(deck) }

      it 'removes the existed Document' do
        expect { removes_deck }.to change(Deck, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_deck
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to dashboard course list' do
        removes_deck
        expect(response).to redirect_to(dashboard_decks_path)
      end
    end
  end
end
