# frozen_string_literal: true

RSpec.describe 'Registration' do
  describe 'GET #new' do
    before { get new_registration_path }

    it 'renders a successful response' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:valid_form_params) do
        {
          email: 'test@example.com',
          password: 'Qwerty123!!!',
          password_confirmation: 'Qwerty123!!!',
          terms_of_service: '1'
        }
      end

      let(:registration) { post registration_path, params: { user_registration_form: valid_form_params } }

      it 'creates a new User' do
        expect { registration }.to change(User, :count).by(1)
      end

      it 'returns status found' do
        registration
        expect(response).to have_http_status(:found)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_form_params) do
        {
          email: '',
          password: 'Qwerty1',
          password_confirmation: 'Qwerty1',
          terms_of_service: '1'
        }
      end

      let(:registration) { post registration_path, params: { user_registration_form: invalid_form_params } }

      it 'does not create a new user' do
        expect { :registration }.not_to change(User, :count)
      end

      it 'returns status unprocessable_entity' do
        registration
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
