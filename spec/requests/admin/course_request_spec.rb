# frozen_string_literal: true

RSpec.describe 'Admin::Course' do
  let(:course) { create(:course, user:) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    let(:courses) { create_list(:course, 3, user:) }

    before do
      courses
      get admin_courses_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns course titles' do
      course_titles = Course.pluck(:title)
      expect(response.body).to include(*course_titles)
    end
  end

  describe 'GET #show' do
    before { get admin_course_path(course) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns course title' do
      expect(response.body).to include(course.title)
    end
  end

  describe 'GET #edit' do
    before { get edit_admin_course_path(course) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put admin_course_path(course),
            params: { course: { title: '  new course  ', description: '  new description   ' } }
      end

      it 'saves changes in the database' do
        expect(course.reload).to have_attributes({ title: 'New course', description: 'New description' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin course list' do
        expect(response).to redirect_to(admin_courses_path)
      end
    end

    context 'with invalid attributes' do
      before { put admin_course_path(course), params: { course: attributes_for(:invalid_course) } }

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when state is archived' do
      let!(:archived_course) { create(:course, :archived, user:) }
      let(:removes_course) { delete admin_course_path(archived_course) }

      it 'removes the existed Course' do
        expect { removes_course }.to change(Course, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_course
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin course list' do
        removes_course
        expect(response).to redirect_to(admin_courses_path)
      end
    end

    context 'when state is activated' do
      let!(:activated_course) { create(:course, :activated, user:) }
      let(:removes_course) { delete admin_course_path(activated_course) }

      it 'does not remove the existed Course' do
        expect { removes_course }.not_to change(Course, :count)
      end

      it 'returns status found' do
        removes_course
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin course list' do
        removes_course
        expect(response).to redirect_to(admin_courses_path)
      end
    end

    context 'when state is pending' do
      let!(:pending_course) { create(:course, user:) }
      let(:removes_course) { delete admin_course_path(pending_course) }

      it 'removes the existed Course' do
        expect { removes_course }.to change(Course, :count).by(-1)
      end

      it 'returns status found' do
        removes_course
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin course list' do
        removes_course
        expect(response).to redirect_to(admin_courses_path)
      end
    end
  end
end
