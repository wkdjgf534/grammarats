# frozen_string_literal: true

RSpec.describe 'Admin::Document' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:category) { create(:category) }
  let(:article) { create(:post, user:, document:, category:) }

  before { login(user) }

  describe 'GET #index' do
    before do
      article
      get admin_posts_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns post title' do
      expect(response.body).to include(article.title)
    end
  end

  describe 'GET #show' do
    before do
      get admin_post_path(article)
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns post title' do
      expect(response.body).to include(article.title)
    end
  end

  describe 'GET #edit' do
    before { get edit_admin_post_path(article) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put admin_post_path(article), params: { post: { title: '  article title  ' } }
      end

      it 'saves changes in the database' do
        expect(article.reload).to have_attributes({ title: 'Article title', slug: 'article-title' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin post list' do
        expect(response).to redirect_to(admin_posts_path)
      end
    end

    context 'with invalid attributes' do
      before do
        put admin_post_path(article), params: { post: attributes_for(:invalid_post) }
      end

      it 'does not save changes in the database' do
        expect(article.reload).to have_attributes({ title: article.title, slug: article.slug })
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { article }

    context 'when successful' do
      let(:removes_post) { delete admin_post_path(article) }

      it 'removes the existed Post' do
        expect { removes_post }.to change(Post, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_post
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin post list' do
        removes_post
        expect(response).to redirect_to(admin_posts_path)
      end
    end
  end
end
