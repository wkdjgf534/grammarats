# frozen_string_literal: true

RSpec.describe 'Admin::Role' do
  let(:student_role) { create(:student_role) }
  let(:teacher_role) { create(:teacher_role) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    before do
      student_role
      teacher_role
      get admin_roles_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns role names' do
      role_names = Role.pluck(:name)
      expect(response.body).to include(*role_names)
    end
  end

  describe 'GET #new' do
    before { get new_admin_role_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_role) { post admin_roles_path, params: { role: attributes_for(:moderator_role) } }

      it 'creates a new Role' do
        expect { create_role }.to change(Role, :count).by(1)
      end

      it 'returns status found' do
        create_role
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin roles list' do
        create_role
        expect(response).to redirect_to(admin_roles_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_role) { post admin_roles_path, params: { role: attributes_for(:invalid_role) } }

      it 'does not create a new Role' do
        expect { create_role }.not_to change(Role, :count)
      end

      it 'returns status unprocessable_entity' do
        create_role
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #show' do
    before do
      student_role
      get admin_role_path(student_role)
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns role name' do
      expect(response.body).to include(student_role.name)
    end
  end

  describe 'GET #edit' do
    before { get edit_admin_role_path(student_role) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put admin_role_path(student_role),
            params: { role: { name: '  new student role  ', description: '  new description   ' } }
      end

      it 'saves changes in the database' do
        expect(student_role.reload).to have_attributes(
          { name: 'New student role', description: 'New description' }
        )
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin roles list' do
        expect(response).to redirect_to(admin_roles_path)
      end
    end

    context 'with invalid attributes' do
      it 'returns status unprocessable_entity' do
        put admin_role_path(student_role), params: { role: attributes_for(:invalid_role) }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { student_role }

    context 'when state is pending' do
      let(:removes_role) { delete admin_role_path(student_role) }

      it 'removes the existed role' do
        expect { removes_role }.to change(Role, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_role
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin roles list' do
        removes_role
        expect(response).to redirect_to(admin_roles_path)
      end
    end

    context 'when state is archived' do
      before { student_role.mark_as_archived! }

      let(:removes_role) { delete admin_role_path(student_role) }

      it 'removes the existed Role' do
        expect { removes_role }.to change(Role, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_role
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin roles list' do
        removes_role
        expect(response).to redirect_to(admin_roles_path)
      end
    end

    context 'when state is activated' do
      let(:removes_role) { delete admin_role_path(student_role) }

      before { student_role.mark_as_activated! }

      it 'does not remove the existed Role' do
        expect { removes_role }.not_to change(Role, :count)
      end

      it 'returns status found' do
        removes_role
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin Roles list' do
        removes_role
        expect(response).to redirect_to(admin_roles_path)
      end
    end
  end
end
