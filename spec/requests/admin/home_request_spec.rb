# frozen_string_literal: true

RSpec.describe 'Admin::Home' do
  describe 'GET #main' do
    it 'returns status ok' do
      get admin_root_path
      expect(response).to have_http_status(:found)
    end
  end
end
