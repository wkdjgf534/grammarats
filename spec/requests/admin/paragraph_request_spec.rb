# frozen_string_literal: true

RSpec.describe 'Admin::Paragraph' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:paragraph) { create(:paragraph, document:) }

  describe 'GET #new' do
    before { get new_admin_document_paragraph_path(document) }

    it 'returns status ok' do
      expect(response).to have_http_status(:found)
    end
  end

  describe 'POST #create'

  describe 'GET #edit' do
    before { get edit_admin_paragraph_path(paragraph) }

    it 'returns status ok' do
      expect(response).to have_http_status(:found)
    end
  end

  describe 'PUT #update' do
  end
end
