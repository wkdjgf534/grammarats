# frozen_string_literal: true

RSpec.describe 'Admin::Deck' do
  let(:deck) { create(:deck, user:) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    let(:decks) { create_list(:deck, 3, user:) }

    before do
      decks
      get admin_decks_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns deck titles' do
      deck_titles = Deck.pluck(:title)
      expect(response.body).to include(*deck_titles)
    end
  end

  describe 'GET #show' do
    before { get admin_deck_path(deck) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns course title' do
      expect(response.body).to include(deck.title)
    end
  end

  describe 'GET #edit' do
    before { get edit_admin_deck_path(deck) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put admin_deck_path(deck),
            params: { deck: { title: '  new deck of cards  ', description: '  new description   ' } }
      end

      it 'saves changes in the database' do
        expect(deck.reload).to have_attributes({ title: 'New deck of cards', description: 'New description' })
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin deck list' do
        expect(response).to redirect_to(admin_decks_path)
      end
    end

    context 'with invalid attributes' do
      before { put admin_deck_path(deck), params: { deck: attributes_for(:invalid_deck) } }

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when state is archived' do
      let!(:archived_deck) { create(:deck, :archived, user:) }
      let(:removes_deck) { delete admin_deck_path(archived_deck) }

      it 'removes the existed Deck' do
        expect { removes_deck }.to change(Deck, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_deck
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin deck list' do
        removes_deck
        expect(response).to redirect_to(admin_decks_path)
      end
    end

    context 'when state is activated' do
      let!(:activated_deck) { create(:deck, :activated, user:) }
      let(:removes_deck) { delete admin_deck_path(activated_deck) }

      it 'does not remove the existed Deck' do
        expect { removes_deck }.not_to change(Deck, :count)
      end

      it 'returns status found' do
        removes_deck
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin deck list' do
        removes_deck
        expect(response).to redirect_to(admin_decks_path)
      end
    end

    context 'when state is pending' do
      let!(:pending_deck) { create(:deck, user:) }
      let(:removes_deck) { delete admin_deck_path(pending_deck) }

      it 'removes the existed Course' do
        expect { removes_deck }.to change(Deck, :count).by(-1)
      end

      it 'returns status found' do
        removes_deck
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin deck list' do
        removes_deck
        expect(response).to redirect_to(admin_decks_path)
      end
    end
  end
end
