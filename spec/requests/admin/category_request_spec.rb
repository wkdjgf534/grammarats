# frozen_string_literal: true

RSpec.describe 'Admin::Category' do
  let(:category) { create(:category) }
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #index' do
    let!(:categories) { create_list(:category, 3) }

    before { get admin_categories_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns category titles' do
      category_titles = categories.pluck(:title)
      expect(response.body).to include(*category_titles)
    end
  end

  describe 'GET #new' do
    before { get new_admin_category_path }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:create_category) { post admin_categories_path, params: { category: attributes_for(:category) } }

      it 'creates a new Category' do
        expect { create_category }.to change(Category, :count).by(1)
      end

      it 'returns status found' do
        create_category
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin category list' do
        create_category
        expect(response).to redirect_to(admin_categories_path)
      end
    end

    context 'with invalid attributes' do
      let(:create_category) { post admin_categories_path, params: { category: attributes_for(:invalid_category) } }

      it 'does not create a new Category' do
        expect { create_category }.not_to change(Category, :count)
      end

      it 'returns status unprocessable_entity' do
        create_category
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET #edit' do
    before { get edit_admin_category_path(category) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'when state is activated' do
      context 'with valid attributes' do
        before do
          put admin_category_path(category),
              params: { category: { title: '  english language  ', description: '  description  ' } }
        end

        it 'returns status unprocessable_entity' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'with invalid attributes' do
        before { put admin_category_path(category), params: { category: attributes_for(:invalid_category) } }

        it 'returns status unprocessable_entity' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    context 'when state is archived' do
      before { category.mark_as_archived! }

      context 'with valid attributes' do
        before do
          put admin_category_path(category),
              params: { category: { title: '  english language  ', description: '  description  ' } }
        end

        it 'saves changes in the database' do
          expect(category.reload).to have_attributes(
            { title: 'English language', description: 'Description', slug: 'english-language' }
          )
        end

        it 'returns status found' do
          expect(response).to have_http_status(:found)
        end

        it 'redirects to admin categories list' do
          expect(response).to redirect_to(admin_categories_path)
        end
      end

      context 'with invalid attributes' do
        before { put admin_category_path(category), params: { category: attributes_for(:invalid_category) } }

        it 'returns status unprocessable_entity' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when state is archived' do
      let!(:archived_category) { create(:category, :archived) }
      let(:removes_category) { delete admin_category_path(archived_category) }

      it 'removes the existed Category' do
        expect { removes_category }.to change(Category, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_category
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin categories list' do
        removes_category
        expect(response).to redirect_to(admin_categories_path)
      end
    end

    context 'when state is activated' do
      let!(:activated_category) { create(:category) }
      let(:removes_category) { delete admin_category_path(activated_category) }

      it 'does not remove the existed Category' do
        expect { removes_category }.not_to change(Category, :count)
      end

      it 'returns status found' do
        removes_category
        expect(response).to have_http_status(:found)
      end

      it 'redirect to admin categories list' do
        removes_category
        expect(response).to redirect_to(admin_categories_path)
      end
    end
  end
end
