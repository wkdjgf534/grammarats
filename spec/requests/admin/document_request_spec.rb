# frozen_string_literal: true

RSpec.describe 'Admin::Document' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }

  before { login(user) }

  describe 'GET #index' do
    let(:documents) { create_list(:document, 3, user:) }

    before do
      documents
      get admin_documents_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns document titles' do
      document_titles = documents.pluck(:title)
      expect(response.body).to include(*document_titles)
    end
  end

  describe 'GET #show' do
  end

  describe 'GET #edit' do
    before { get edit_admin_document_path(document) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      before do
        put admin_document_path(document),
            params: { document: { title: '  english language  ', description: '  description  ' } }
      end

      it 'saves changes in the database' do
        expect(document.reload).to have_attributes(
          { title: 'English language', description: 'Description' }
        )
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'redirects to admin document list' do
        expect(response).to redirect_to(admin_documents_path)
      end
    end

    context 'with invalid attributes' do
      before do
        put admin_document_path(document),
            params: { document: attributes_for(:invalid_document) }
      end

      it 'does not save changes in the database' do
        expect(document.reload).to have_attributes(
          { title: document.title, description: document.description }
        )
      end

      it 'returns status unprocessable_entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    before { document }

    context 'when successful' do
      let(:removes_document) { delete admin_document_path(document) }

      it 'removes the existed Document' do
        expect { removes_document }.to change(Document, :count).by(-1)
      end

      it 'returns status see_other' do
        removes_document
        expect(response).to have_http_status(:see_other)
      end

      it 'redirect to admin document list' do
        removes_document
        expect(response).to redirect_to(admin_documents_path)
      end
    end
  end
end
