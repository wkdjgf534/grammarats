# frozen_string_literal: true

RSpec.describe 'Post' do
  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:category) { create(:category) }

  describe 'GET #index' do
    let(:articles) { create_list(:post, 3, :activated, user:, document:, category:) }

    before do
      articles
      get posts_path
    end

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns post titles' do
      article_titles = Post.pluck(:title)
      expect(response.body).to include(*article_titles)
    end
  end

  describe 'GET #show' do
    let(:article) { create(:post, :activated, user:, document:, category:) }

    before { get post_path(article) }

    it 'returns status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns post title' do
      expect(response.body).to include(article.title)
    end
  end
end
