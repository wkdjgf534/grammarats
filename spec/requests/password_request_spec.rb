# frozen_string_literal: true

RSpec.describe 'Registration' do
  let(:user) { create(:user) }

  before { login(user) }

  describe 'GET #edit' do
    it 'renders a successful response' do
      get edit_password_path
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT #update' do
    context 'with valid attributes' do
      let(:valid_form_params) do
        { password_challenge: 'Qwerty123!', password: 'Qwerty123!!', password_confirmation: 'Qwerty123!!' }
      end

      it 'returns status found' do
        put password_path, params: { user_password_form: valid_form_params }
        expect(response).to have_http_status(:found)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_form_params) do
        { password_challenge: 'Qwerty123!', password: 'Q', password_confirmation: 'Q' }
      end

      it 'returns status unprocessable_entity' do
        put password_path, params: { user_password_form: invalid_form_params }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
