# frozen_string_literal: true

RSpec.describe 'Reset Password' do
  let(:user) { create(:user) }
  let(:token) { user.generate_token_for(:password_reset) }

  describe 'GET #new' do
    before { get edit_reset_password_path }

    it 'renders a successful response' do
      expect(response).to have_http_status(:ok)
    end

    describe 'PUT #update' do
      before { user }

      context 'with valid attributes' do
        let(:valid_form_params) { { password: 'Qwerty123!!!', password_confirmation: 'Qwerty123!!!' } }

        it 'returns status found' do
          put reset_password_path(token:), params: { user_reset_password_form: valid_form_params }
          expect(response).to have_http_status(:found)
        end
      end

      context 'with invalid attributes' do
        context 'with invalid attributes' do
          let(:invalid_form_params) { { password: 'Q', password_confirmation: 'Q' } }

          it 'returns status unprocessable_entity' do
            put reset_password_path(token:), params: { user_reset_password_form: invalid_form_params }
            expect(response).to have_http_status(:unprocessable_entity)
          end
        end
      end
    end
  end
end
