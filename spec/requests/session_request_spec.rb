# frozen_string_literal: true

RSpec.describe 'Session' do
  let(:user) { create(:user) }

  describe 'GET #new' do
    it 'returns status ok' do
      get new_session_path
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      let(:valid_form_params) { { email: user.email, password: 'Qwerty123!' } }

      it 'returns status found' do
        post session_path, params: { user_sign_in_form: valid_form_params }
        expect(response).to have_http_status(:found)
      end
    end

    context 'with invalid attributes' do
      let(:invalid_form_params) { { email: '', password: 'Qwerty12345' } }

      it 'returns status unprocessable_entity' do
        post session_path, params: { user_sign_in_form: invalid_form_params }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when sign out' do
      before do
        login(user)
        delete session_path(user)
      end

      it 'returns status found' do
        expect(response).to have_http_status(:found)
      end

      it 'returns nil in session' do
        expect(session[:user_id]).to be_nil
      end
    end
  end
end
