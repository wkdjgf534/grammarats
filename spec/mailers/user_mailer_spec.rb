# frozen_string_literal: true

RSpec.describe UserMailer do
  describe '#reset_password' do
    let(:user) { build(:user) }
    let(:token) { user.generate_token_for(:password_reset) }
    let(:mail) { described_class.reset_password(user:, token:).deliver_now }

    context 'when send_mail' do
      it 'renders the subject' do
        expect(mail.subject).to eq('Reset password')
      end

      it 'renders the receiver email' do
        expect(mail.to).to eq([ user.email ])
      end

      it 'renders the sender email' do
        expect(mail.from).to eq([ 'noreply@grammarats.info' ])
      end
    end
  end
end
