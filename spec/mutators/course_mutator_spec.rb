# frozen_string_literal: true

RSpec.describe CourseMutator do
  let(:user) { create(:user) }

  describe '.create' do
    subject(:create_course) { described_class.create!(params) }

    let(:params) do
      { course_params: { title: 'New title', description: 'New description' }, user: }
    end

    it 'creates a new record' do
      expect { create_course }.to change(Course, :count).by(1)
    end
  end

  describe '.update!' do
    subject(:update_course) { described_class.update!(params) }

    let(:course) { create(:course, user:) }

    let(:course_params) do
      { title: 'Updated title', description: 'Updated description' }
    end

    let(:params) { { course:, course_params: } }

    it 'does save changes in the database' do
      update_course
      expect(course.reload).to have_attributes(
        {
          title: 'Updated title',
          description: 'Updated description'
        }
      )
    end
  end

  describe '.destroy!' do
    subject(:destroy_course) { described_class.destroy!(params) }

    let(:course) { create(:course, user:) }

    let(:params) { { course: } }

    it 'removes course' do
      course
      expect { destroy_course }.to change(Course, :count).by(-1)
    end
  end
end
