# frozen_string_literal: true

RSpec.describe PermissionMutator do
  let(:role) { create(:teacher_role, :activated) }

  describe '.create' do
    subject(:create_permission) { described_class.create!(params) }

    let(:params) do
      { entity: 'Admin/Roles', verb: 'index', is_active: true, role: }
    end

    it 'creates a new record' do
      expect { create_permission }.to change(Permission, :count).by(1)
    end
  end
end
