# frozen_string_literal: true

RSpec.describe DeckMutator do
  let(:user) { create(:user) }

  describe '.create' do
    subject(:create_deck) { described_class.create!(params) }

    let(:params) do
      { deck_params: { title: 'New title', description: 'New description' }, user: }
    end

    it 'creates a new record' do
      expect { create_deck }.to change(Deck, :count).by(1)
    end
  end

  describe '.update!' do
    subject(:update_deck) { described_class.update!(params) }

    let(:deck) { create(:deck, user:) }

    let(:deck_params) do
      { title: 'Updated title', description: 'Updated description' }
    end

    let(:params) { { deck:, deck_params: } }

    it 'does save changes in the database' do
      update_deck
      expect(deck.reload).to have_attributes(
        {
          title: 'Updated title',
          description: 'Updated description'
        }
      )
    end
  end

  describe '.destroy!' do
    subject(:destroy_deck) { described_class.destroy!(params) }

    let(:deck) { create(:deck, user:) }

    let(:params) { { deck: } }

    it 'removes deck' do
      deck
      expect { destroy_deck }.to change(Deck, :count).by(-1)
    end
  end
end
