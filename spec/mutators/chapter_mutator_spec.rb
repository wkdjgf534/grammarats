# frozen_string_literal: true

RSpec.describe ChapterMutator do
  let(:course) { create(:course, user:) }
  let(:user) { create(:user) }

  describe '.create' do
    subject(:create_chapter) { described_class.create!(params) }

    let(:params) do
      { title: 'New chapter', position: 0, course: }
    end

    it 'creates a new record' do
      expect { create_chapter }.to change(Chapter, :count).by(1)
    end
  end
end
