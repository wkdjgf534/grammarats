# frozen_string_literal: true

RSpec.describe CategoryMutator do
  let(:category) { create(:category) }

  before { category }

  describe '.update!' do
    subject(:update_category) { described_class.update!(params) }

    let(:category_params) do
      { title: 'Updated title', description: 'Updated description' }
    end

    let(:params) { { category:, category_params: } }

    it 'does save changes in the database' do
      update_category
      expect(category.reload).to have_attributes(
        {
          title: 'Updated title',
          description: 'Updated description'
        }
      )
    end
  end

  describe '.destroy!' do
    subject(:destroy_category) { described_class.destroy!(params) }

    let(:params) { { category: } }

    it 'removes category' do
      expect { destroy_category }.to change(Category, :count).by(-1)
    end
  end
end
