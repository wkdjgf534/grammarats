# frozen_string_literal: true

RSpec.describe ApplicationHelper do
  let(:app_name) { Rails.application.class.module_parent.to_s }

  describe '#application_title' do
    it 'returns the application name' do
      expect(helper.application_title).to eq(app_name)
    end
  end

  describe '#full_title' do
    context 'with page_title' do
      it 'returns a string' do
        expect(helper.full_title('Test')).to eq("#{app_name} | Test")
      end
    end
  end
end
