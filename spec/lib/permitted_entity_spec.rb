# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PermittedEntity do
  describe '.controllers_with_actions' do
    it 'returns an array of data' do
      expect(described_class.controllers_with_actions).to include(
        {
          entity: 'Admin/Roles',
          verbs: %w[index create new edit show update update destroy]
        }
      )
    end
  end
end
