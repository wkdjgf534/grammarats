# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DropdownAggregator do
  describe '.values' do
    it 'returns an array for Document dropdown' do
      documents = [
        [ 'Lesson', 'lesson' ],
        [ 'Post', 'post' ],
        [ 'Legal agreement', 'legal_agreement' ],
        [ 'Privacy policy', 'privacy_policy' ],
        [ 'Terms of service', 'terms_of_service' ]
      ].freeze
      expect(described_class.values(enum: Document.kinds)).to eq(documents)
    end
  end
end
