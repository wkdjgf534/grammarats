# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Document do
  subject(:document) { build(:document, user:) }

  let(:user) { create(:user) }

  it_behaves_like 'DocumentRepository'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates presence', :title
      it_behaves_like 'validates min length',
                      :title,
                      Faker::Lorem.characters(number: Document::TITLE[:min] - 1)

      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Document::TITLE[:max] + 1)
    end

    context 'with description' do
      it_behaves_like 'validates max length',
                      :description,
                      Faker::Lorem.characters(number: Document::DESCRIPTION[:max] + 1)
    end

    context 'with kind' do
      # TODO: add unit test
    end
  end
end
