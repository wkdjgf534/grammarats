# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course do
  subject(:course) { build(:course, user:) }

  let(:user) { create(:user) }

  it_behaves_like 'CourseRepository'
  it_behaves_like 'CourseStateMachine'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates presence', :title
      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Course::TITLE[:max] + 1)
    end

    context 'with description' do
      it_behaves_like 'validates max length',
                      :description,
                      Faker::Lorem.characters(number: Course::DESCRIPTION[:max] + 1)
    end

    context 'with state' do
      it_behaves_like 'validates presence', :state

      context 'when valid value' do
        %i[pending archived activated].each do |valid_status|
          it "is valid with 'pending', 'archived', 'activated'" do
            expect(build(:course, user:, state: valid_status)).to be_valid
          end
        end
      end

      context 'when invalid value' do
        it "is invalid without 'pending', 'archived', 'activated'" do
          expect(build(:course, user:, state: :test)).not_to be_valid
        end
      end
    end
  end
end
