# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Chapter do
  subject(:chapter) { build(:chapter, course:) }

  let(:course) { create(:course, user:) }
  let(:user) { create(:user) }

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Chapter::TITLE[:max] + 1)
    end

    context 'with position' do
      it_behaves_like 'validates presence', :position
    end
  end
end
