# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Role do
  subject(:student) { build(:student_role) }

  it_behaves_like 'RoleRepository'
  it_behaves_like 'RoleStateMachine'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with name' do
      it_behaves_like 'validates presence', :name
      it_behaves_like 'validates min length',
                      :name,
                      Faker::Lorem.characters(number: Role::NAME[:min] - 1)

      it_behaves_like 'validates max length',
                      :name,
                      Faker::Lorem.characters(number: Role::NAME[:max] + 1)

      it 'is not valid, must be unique title' do
        create(:student_role, name: student.name)
        expect(student.save).not_to be_truthy
      end
    end
  end
end
