# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category do
  subject(:category) { build(:category) }

  it_behaves_like 'CategoryRepository'
  it_behaves_like 'CategoryStateMachine'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates presence', :title
      it_behaves_like 'validates min length',
                      :title,
                      Faker::Lorem.characters(number: Category::TITLE[:min] - 1)

      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Category::TITLE[:max] + 1)

      it 'is not valid, must be unique title' do
        create(:category, title: category.title)
        expect(category.save).not_to be_truthy
      end
    end

    context 'with state' do
      it_behaves_like 'validates presence', :state

      context 'when valid value' do
        %i[activated archived].each do |valid_status|
          it "is valid with 'activated', 'archived'" do
            expect(build(:category, state: valid_status)).to be_valid
          end
        end
      end

      context 'when invalid value' do
        it "is invalid without 'activated', 'archived'" do
          expect(build(:category, state: :test)).not_to be_valid
        end
      end
    end

    context 'with description' do
      it_behaves_like 'validates max length',
                      :description,
                      Faker::Lorem.characters(number: Category::DESCRIPTION[:max] + 1)
    end
  end
end
