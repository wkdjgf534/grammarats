# frozen_string_literal: true

RSpec.describe User do
  subject(:user) { build(:user) }

  it_behaves_like 'UserRepository'
  it_behaves_like 'UserStateMachine'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with state' do
      it_behaves_like 'validates presence', :state

      context 'when valid value' do
        %i[activated banned deleted].each do |valid_status|
          it "is valid with 'activated', 'banned' 'deleted'" do
            expect(build(:user, state: valid_status)).to be_valid
          end
        end
      end

      context 'when invalid value' do
        it "is invalid without 'activated', 'banned' 'deleted'" do
          expect(build(:user, state: :test)).not_to be_valid
        end
      end
    end
  end

  describe '#guest?' do
    it { is_expected.not_to be_guest }
  end
end
