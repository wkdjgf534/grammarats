# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post do
  subject(:post) { build(:post, user:, category:, document:) }

  let(:user) { create(:user) }
  let(:document) { create(:document, user:) }
  let(:category) { create(:category) }

  it_behaves_like 'PostRepository'
  it_behaves_like 'PostStateMachine'

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates presence', :title
      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Post::TITLE[:max] + 1)
    end

    context 'with state' do
      it_behaves_like 'validates presence', :state

      context 'when valid value' do
        %i[pending activated archived].each do |valid_status|
          it "is valid with 'pending', 'archived', 'activated'" do
            expect(build(:post, user:, category:, document:, state: valid_status)).to be_valid
          end
        end
      end

      context 'when invalid value' do
        it "is invalid without 'pending', 'archived', 'activated'" do
          expect(build(:post, user:, category:, document:, state: :test)).not_to be_valid
        end
      end
    end
  end
end
