# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Paragraph do
  subject(:paragraph) { build(:paragraph, document:) }

  let(:document) { create(:document, user:) }
  let(:user) { create(:user) }

  describe 'Validations' do
    it { is_expected.to be_valid }

    context 'with title' do
      it_behaves_like 'validates max length',
                      :title,
                      Faker::Lorem.characters(number: Paragraph::TITLE[:max] + 1)
    end

    context 'with content' do
      it_behaves_like 'validates presence', :content
    end

    context 'with position' do
      it_behaves_like 'validates presence', :position
    end
  end
end
