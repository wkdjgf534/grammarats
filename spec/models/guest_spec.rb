# frozen_string_literal: true

RSpec.describe Guest do
  subject(:guest) { described_class.new }

  describe '#id' do
    it 'returns a fictional id for guest' do
      expect(guest.id).to be_present
    end
  end

  describe 'email' do
    it 'returns an empty email' do
      expect(guest.email).to be_nil
    end
  end

  describe 'guest?' do
    it { is_expected.to be_guest }
  end

  describe 'full_name' do
    it 'returns a full name' do
      expect(guest.full_name).to eq('Guest user')
    end
  end
end
