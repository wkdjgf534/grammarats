# frozen_string_literal: true

module ControllerHelpers
  def login(user)
    post(
      session_url,
      params: { user_sign_in_form: { email: user.email, password: user.password } }
    )
  end
end
