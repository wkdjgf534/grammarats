# frozen_string_literal: true

shared_examples 'CategoryStateMachine' do
  it { is_expected.to allow_event(:mark_as_activated).on(:state) }
  it { is_expected.to allow_event(:mark_as_archived).on(:state) }
end
