# frozen_string_literal: true

shared_examples 'RoleStateMachine' do
  it { is_expected.to allow_event(:mark_as_pending).on(:state) }
  it { is_expected.to allow_event(:mark_as_activated).on(:state) }
  it { is_expected.to allow_event(:mark_as_archived).on(:state) }
end
