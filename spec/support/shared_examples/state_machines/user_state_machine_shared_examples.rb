# frozen_string_literal: true

shared_examples 'UserStateMachine' do
  it { is_expected.to allow_event(:mark_as_activated).on(:state) }
  it { is_expected.to allow_event(:mark_as_banned).on(:state) }
end
