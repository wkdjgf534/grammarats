# frozen_string_literal: true

shared_examples 'validates presence' do |field|
  it "validates presence of #{field}" do
    subject.public_send(:"#{field}=", nil)
    expect(subject).not_to be_valid
    expect(subject.errors[field]).to include("can't be blank")
  end
end

shared_examples 'validates min length' do |field, value|
  it "validates min length of #{field}" do
    subject.public_send(:"#{field}=", value)
    expect(subject).not_to be_valid
    expect(subject.errors[field][0]).to include('is too short')
  end
end

shared_examples 'validates max length' do |field, value|
  it "validates max length of #{field}" do
    subject.public_send(:"#{field}=", value)
    expect(subject).not_to be_valid
    expect(subject.errors[field][0]).to include('is too long')
  end
end
