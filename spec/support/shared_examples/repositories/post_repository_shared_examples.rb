# frozen_string_literal: true

shared_examples 'PostRepository' do
  describe 'the interface' do
    it 'has filter_by_author' do
      expect(described_class).to respond_to(:filter_by_author)
    end

    it 'has pending' do
      expect(described_class).to respond_to(:pending)
    end

    it 'has activated' do
      expect(described_class).to respond_to(:activated)
    end

    it 'has archived' do
      expect(described_class).to respond_to(:archived)
    end
  end
end
