# frozen_string_literal: true

shared_examples 'ClassRoomRepository' do
  describe 'the interface' do
    it 'has private' do
      expect(described_class).to respond_to(:get_private)
    end

    it 'has public' do
      expect(described_class).to respond_to(:get_public)
    end

    it 'has filter_by_teacher' do
      expect(described_class).to respond_to(:by_teacher)
    end
  end
end
