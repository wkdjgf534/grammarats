# frozen_string_literal: true

shared_examples 'CourseRepository' do
  describe 'the interface' do
    it 'has pending' do
      expect(described_class).to respond_to(:pending)
    end

    it 'has activated' do
      expect(described_class).to respond_to(:activated)
    end

    it 'has archived' do
      expect(described_class).to respond_to(:archived)
    end
  end
end
