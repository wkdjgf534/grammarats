# frozen_string_literal: true

shared_examples 'DocumentRepository' do
  describe 'the interface' do
    it 'has filter_by_author' do
      expect(described_class).to respond_to(:filter_by_author)
    end

    it 'has filter_by_kind' do
      expect(described_class).to respond_to(:filter_by_kind)
    end
  end
end
