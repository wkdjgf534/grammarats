# frozen_string_literal: true

shared_examples 'UserRepository' do
  describe 'the interface' do
    it 'has activated' do
      expect(described_class).to respond_to(:activated)
    end

    it 'has deleted' do
      expect(described_class).to respond_to(:deleted)
    end

    it 'has banned' do
      expect(described_class).to respond_to(:banned)
    end
  end
end
