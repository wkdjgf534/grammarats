# frozen_string_literal: true

FactoryBot.define do
  factory :student_role, class: 'Role' do
    name { 'Student' }
    description { 'Student role' }

    trait :activated do
      before :create, &:mark_as_activated!
    end

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :teacher_role, class: 'Role' do
    name { 'Teacher' }
    description { 'Teacher role' }

    trait :activated do
      before :create, &:mark_as_activated!
    end

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :moderator_role, class: 'Role' do
    name { 'Moderator' }
    description { 'Moderator role' }

    before :create, &:mark_as_activated!

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :admin_role, class: 'Role' do
    name { 'admin' }
    description { 'admin role' }

    before :create, &:mark_as_activated!

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :invalid_role, class: 'Role' do
    name { '' }
    description { '' }
  end
end
