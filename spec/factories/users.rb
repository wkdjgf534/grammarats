# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:first_name) { Faker::Name.first_name }
    sequence(:last_name) { Faker::Name.last_name }
    sequence(:email) { Faker::Internet.unique.email }
    password { 'Qwerty123!' }
    password_confirmation { 'Qwerty123!' }

    before :create, &:mark_as_activated!

    trait :banned do
      before :create, &:mark_as_banned!
    end

    trait :deleted do
      before :create, &:mark_as_deleted!
    end
  end

  factory :invalid_user, class: 'User' do
    email { '' }
  end
end
