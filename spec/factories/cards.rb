# frozen_string_literal: true

FactoryBot.define do
  factory :card do
    sequence(:title) { |n| "Card title #{n}" }
    sequence(:position) { |n| n }
  end

  factory :invalid_card, class: 'Card' do
    title { nil }
    position { nil }
  end
end
