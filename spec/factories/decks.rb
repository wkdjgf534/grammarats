# frozen_string_literal: true

FactoryBot.define do
  factory :deck do
    sequence(:title) { |n| "Deck title #{n}" }
    sequence(:description) { Faker::Lorem.paragraph }

    trait :activated do
      before :create, &:mark_as_activated!
    end

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :invalid_deck, class: 'Deck' do
    title { '' }
    description { '' }
  end
end
