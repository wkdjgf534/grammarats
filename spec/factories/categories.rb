# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    sequence(:title) { |n| "Category title #{n}" }
    sequence(:description) { Faker::Lorem.paragraph }
    sequence(:slug) { |n| "category-title-#{n}" }

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :invalid_category, class: 'Category' do
    title { '' }
    slug { '' }
    description { '' }
  end
end
