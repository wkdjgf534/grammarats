# frozen_string_literal: true

FactoryBot.define do
  factory :chapter do
    sequence(:title) { |n| "Chapter title #{n}" }
    sequence(:position) { |n| n }
  end

  factory :invalid_chapter, class: 'Chapter' do
    title { nil }
    position { nil }
  end
end
