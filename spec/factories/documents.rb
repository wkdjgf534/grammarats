# frozen_string_literal: true

FactoryBot.define do
  factory :document do
    sequence(:title) { |n| "Document title #{n}" }
    sequence(:description) { Faker::Lorem.paragraph }
  end

  factory :invalid_document, class: 'Document' do
    title { '' }
    description { '' }
  end
end
