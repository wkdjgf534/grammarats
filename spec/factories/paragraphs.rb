# frozen_string_literal: true

FactoryBot.define do
  factory :paragraph do
    sequence(:title) { |n| "Paragraph title #{n}" }
    sequence(:content) { |n| "Paragraph content #{n}" }
    sequence(:position) { |n| n }
  end

  factory :invalid_paragraph, class: 'Paragraph' do
    title { nil }
    content { {} }
    position { nil }
  end
end
