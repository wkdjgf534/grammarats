# frozen_string_literal: true

FactoryBot.define do
  factory :course do
    sequence(:title) { |n| "Course title #{n}" }
    sequence(:description) { Faker::Lorem.paragraph }

    trait :activated do
      before :create, &:mark_as_activated!
    end

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :invalid_course, class: 'Course' do
    title { '' }
    description { '' }
  end
end
