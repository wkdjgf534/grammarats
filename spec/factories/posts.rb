# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    sequence(:title) { |n| "Post title #{n}" }
    sequence(:slug) { |n| "post-title-#{n}" }

    trait :activated do
      before :create, &:mark_as_activated!
    end

    trait :archived do
      before :create, &:mark_as_archived!
    end
  end

  factory :invalid_post, class: 'Post' do
    title { '' }
    slug { '' }
  end
end
