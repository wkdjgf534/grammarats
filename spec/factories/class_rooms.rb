# frozen_string_literal: true

FactoryBot.define do
  factory :class_room do
    sequence(:title) { |n| "Class room title #{n}" }
    sequence(:description) { Faker::Lorem.paragraph }
    is_private { false }
  end

  factory :invalid_class_room, class: 'ClassRoom' do
    title { '' }
    description { '' }
  end
end
