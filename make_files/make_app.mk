brakeman-check:
	bundle exec brakeman -q

bundler-audit:
	bundle exec bundle-audit check

reset:
	bin/rails db:drop
	bin/rails db:create
	bin/rails db:migrate
	bin/rails db:seed

start:
	rm -rf tmp/pids/server.pid
	bin/dev

gems-cleanup:
	gem cleanup

db-consistency:
	bundle exec database_consistency; exit 0

