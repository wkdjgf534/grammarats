## Grammarats

Grammarats is an education platform for people who want to learn or refresh knowledge several languages at once.

### Status

Project is in progress.

### Useful articles

https://confidentcode.com/guides/picking-the-right-search-functionality/

#### View component

https://evilmartians.com/chronicles/viewcomponent-in-the-wild-embracing-tailwindcss-classes-and-html-attributes

#### Modal

https://blog.appsignal.com/2024/02/21/hotwire-modals-in-ruby-on-rails-with-stimulus-and-turbo-frames.html

### Model subscription
https://garrettdimon.com/journal/posts/erb-partials-helpers-and-rails
https://garrettdimon.com/journal/posts/data-modeling-saas-entitlements-and-pricing
https://garrettdimon.com/journal/posts/organizing-rails-code-with-activerecord-associated-objects

### Gitlab
https://docs.gitlab.com/ee/ci/

### TODO

- add folders `teacher\class_room`, `teacher\document` in dashboard directory
- add folders `student\document`, in dashboard directory
- rename paragraph to section or fragmnent (paragraph, delimeter, etc)

### Libraries

https://github.com/DmitryTsepelev/store_model

https://github.com/Casecommons/pg_search
https://github.com/afair/postgresql_cursor
https://github.com/teoljungberg/fx
https://github.com/scenic-views/scenic

https://github.com/DmitryTsepelev/ar_lazy_preload

https://github.com/roidrage/lograge

https://github.com/djezzzl/database_consistency

https://github.com/palkan/isolator
https://github.com/ankane/strong_migrations
https://github.com/pawurb/rails-pg-extras

https://github.com/zombocom/derailed_benchmarks
https://github.com/jhawthorn/vernier

https://github.com/julianrubisch/attractor
https://github.com/danmayer/coverband

for tests
https://github.com/palkan/n_plus_one_control

Exanple
https://github.com/Hexlet/hexlet-cv
