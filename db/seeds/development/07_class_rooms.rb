# frozen_string_literal: true

ClassRoom.destroy_all if ClassRoom.exists?
