# frozen_string_literal: true

Document.destroy_all if Document.exists?

teacher1 = User.find_by(email: 'teacher1@test.com')
teacher2 = User.find_by(email: 'teacher1@test.com')
admin = User.find_by(email: 'admin1@test.com')
moderator = User.find_by(email: 'moderator1@test.com')

authors = [ teacher1, teacher2, admin, moderator ]
documents = []

4.times do |index|
  documents << {
    title: "Document #{index + 1}",
    description: "Document description #{index + 1}",
    user: authors.sample,
    kind: index
  }
end

Document.create!(documents)
