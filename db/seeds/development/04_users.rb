# frozen_string_literal: true

User.destroy_all if User.exists?

PASSWORD = 'Qwerty123!'

users = [
  { email: 'student1@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'student2@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'teacher1@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'teacher2@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'moderator1@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'moderator2@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'admin1@test.com', password: PASSWORD, password_confirmation: PASSWORD },
  { email: 'admin2@test.com', password: PASSWORD, password_confirmation: PASSWORD }
].freeze

User.create!(users)
