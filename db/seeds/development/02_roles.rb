# frozen_string_literal: true

Role.destroy_all if Role.exists?

roles = [
  { name: 'student', description: 'student role', state: 'activated' },
  { name: 'teacher', description: 'teacher role', state: 'activated' },
  { name: 'moderator', description: 'moderator role', state: 'activated' },
  { name: 'admin', description: 'admin role', state: 'activated' }
].freeze

Role.create!(roles)
