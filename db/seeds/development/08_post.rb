# frozen_string_literal: true

Post.destroy_all if Post.exists?
