# frozen_string_literal: true

Category.destroy_all if Category.exists?

CATEGORY_TITLES = [
  'Announcement',
  'Education',
  'Entertainment',
  'Engilsh language',
  'Serbian language',
  'German language',
  'Positive thoughts',
  'Release notes'
].freeze

CATEGORY_STATES = %w[activated archived].freeze

CATEGORY_TITLES.each do |category|
  category_state = CATEGORY_STATES.sample
  Category.create!(
    title: category,
    slug: category.parameterize,
    description: Faker::Lorem.paragraph,
    state: category_state,
    archived_at: (Time.zone.now if category_state == 'archived')
  )
end
