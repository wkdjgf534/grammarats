class CreateCourses < ActiveRecord::Migration[7.1]
  def change
    create_table :courses, id: :uuid do |t|
      t.string :title, null: false, limit: 255, index: true
      t.string :description, limit: 16_384
      t.string :state, null: false, limit: 100
      t.string :archived_at
      t.string :activated_at
      t.integer :chapters_count, default: 0
      t.references :user, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
