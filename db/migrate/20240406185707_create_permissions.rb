class CreatePermissions < ActiveRecord::Migration[7.1]
  def change
    create_table :permissions do |t|
      t.string :entity, null: false, limit: 255
      t.string :verb, null: false, limit: 50
      t.boolean :is_active, null: false, default: false
      t.references :role, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
