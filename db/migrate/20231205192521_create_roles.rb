class CreateRoles < ActiveRecord::Migration[7.1]
  def change
    create_table :roles, id: :uuid do |t|
      t.string :name, null: false, limit: 255, index: { unique: true }
      t.text :description, limit: 16_384
      t.string :archived_at
      t.string :activated_at
      t.string :state, null: false, limit: 100

      t.timestamps
    end
  end
end
