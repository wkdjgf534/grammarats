class CreatePosts < ActiveRecord::Migration[7.1]
  def change
    create_table :posts do |t|
      t.string :title, limit: 255, null: false, index: true
      t.string :slug, limit: 300, null: false
      t.string :state, null: false, limit: 100
      t.string :archived_at
      t.string :activated_at
      t.references :category, null: false, index: true, foreign_key: true
      t.references :user, null: false, index: true, foreign_key: true, type: :uuid
      t.references :document, null: false, index: true, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
