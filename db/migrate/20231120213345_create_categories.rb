class CreateCategories < ActiveRecord::Migration[7.1]
  def change
    create_table :categories do |t|
      t.string :title, limit: 255, null: false, index: { unique: true }
      t.string :slug, limit: 300, null: false
      t.text :description, limit: 16_384
      t.string :archived_at
      t.string :state, null: false, limit: 100
      t.integer :posts_count, null: false, default: 0

      t.timestamps
    end
  end
end
