class CreateDocuments < ActiveRecord::Migration[7.1]
  def change
    create_table :documents, id: :uuid do |t|
      t.string :title, null: false, limit: 255, index: true
      t.text :description, limit: 16_384
      t.integer :kind, null: false, default: 0
      t.integer :paragraphs_count, null: false, default: 0
      t.references :user, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
