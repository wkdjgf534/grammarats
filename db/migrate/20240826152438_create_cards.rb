class CreateCards < ActiveRecord::Migration[7.1]
  def change
    create_table :cards do |t|
      t.string :title, limit: 255
      t.integer :position, null: false
      t.references :deck, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
