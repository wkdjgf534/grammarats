class CreateParagraphs < ActiveRecord::Migration[7.1]
  def change
    create_table :paragraphs do |t|
      t.string :title, limit: 255
      t.jsonb :content, null: false, default: {}
      t.integer :position,  null: false
      t.references :document, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
