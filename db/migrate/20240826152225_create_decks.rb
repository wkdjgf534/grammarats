class CreateDecks < ActiveRecord::Migration[7.1]
  def change
    create_table :decks, id: :uuid do |t|
      t.string :title, limit: 255, null: false, index: true
      t.string :description, limit: 16_384
      t.string :archived_at
      t.string :activated_at
      t.string :state, null: false, limit: 100
      t.references :user, null: false, index: true, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
