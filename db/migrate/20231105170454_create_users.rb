class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users, id: :uuid do |t|
      t.string :first_name, limit: 200
      t.string :last_name, limit: 200
      t.string :email, limit: 200, null: false, index: { unique: true }
      t.string :password_digest
      t.string :state, null: false, limit: 100
      t.integer :posts_count, default: 0
      t.timestamp :activated_at
      t.timestamp :deleted_at
      t.timestamp :banned_at

      t.timestamps
    end
  end
end
