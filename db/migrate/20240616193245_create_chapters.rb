class CreateChapters < ActiveRecord::Migration[7.1]
  def change
    create_table :chapters do |t|
      t.string :title, limit: 255
      t.integer :position, null: false
      t.references :course, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
