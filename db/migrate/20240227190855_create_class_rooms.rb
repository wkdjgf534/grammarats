class CreateClassRooms < ActiveRecord::Migration[7.1]
  def change
    create_table :class_rooms, id: :uuid do |t|
      t.string :title, null: false, limit: 255, index: true
      t.text :description, limit: 16_384
      t.boolean :is_private, null: false, default: false
      t.references :user, null: false, foreign_key: true, index: true, type: :uuid

      t.timestamps
    end
  end
end
