# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.2].define(version: 2024_08_26_152438) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "cards", force: :cascade do |t|
    t.string "title", limit: 255
    t.integer "position", null: false
    t.uuid "deck_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deck_id"], name: "index_cards_on_deck_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "slug", limit: 300, null: false
    t.text "description"
    t.string "archived_at"
    t.string "state", limit: 100, null: false
    t.integer "posts_count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_categories_on_title", unique: true
  end

  create_table "chapters", force: :cascade do |t|
    t.string "title", limit: 255
    t.integer "position", null: false
    t.uuid "course_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_chapters_on_course_id"
  end

  create_table "class_rooms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.text "description"
    t.boolean "is_private", default: false, null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_class_rooms_on_title"
    t.index ["user_id"], name: "index_class_rooms_on_user_id"
  end

  create_table "courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "description", limit: 16384
    t.string "state", limit: 100, null: false
    t.string "archived_at"
    t.string "activated_at"
    t.integer "chapters_count", default: 0
    t.uuid "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_courses_on_title"
    t.index ["user_id"], name: "index_courses_on_user_id"
  end

  create_table "decks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "description", limit: 16384
    t.string "archived_at"
    t.string "activated_at"
    t.string "state", limit: 100, null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_decks_on_title"
    t.index ["user_id"], name: "index_decks_on_user_id"
  end

  create_table "documents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.text "description"
    t.integer "kind", default: 0, null: false
    t.integer "paragraphs_count", default: 0, null: false
    t.uuid "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_documents_on_title"
    t.index ["user_id"], name: "index_documents_on_user_id"
  end

  create_table "paragraphs", force: :cascade do |t|
    t.string "title", limit: 255
    t.jsonb "content", default: {}, null: false
    t.integer "position", null: false
    t.uuid "document_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["document_id"], name: "index_paragraphs_on_document_id"
  end

  create_table "permissions", force: :cascade do |t|
    t.string "entity", limit: 255, null: false
    t.string "verb", limit: 50, null: false
    t.boolean "is_active", default: false, null: false
    t.uuid "role_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_permissions_on_role_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title", limit: 255, null: false
    t.string "slug", limit: 300, null: false
    t.string "state", limit: 100, null: false
    t.string "archived_at"
    t.string "activated_at"
    t.bigint "category_id", null: false
    t.uuid "user_id", null: false
    t.uuid "document_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_posts_on_category_id"
    t.index ["document_id"], name: "index_posts_on_document_id"
    t.index ["title"], name: "index_posts_on_title"
    t.index ["user_id"], name: "index_posts_on_user_id"
  end

  create_table "roles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", limit: 255, null: false
    t.text "description"
    t.string "archived_at"
    t.string "activated_at"
    t.string "state", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_roles_on_name", unique: true
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "first_name", limit: 200
    t.string "last_name", limit: 200
    t.string "email", limit: 200, null: false
    t.string "password_digest"
    t.string "state", limit: 100, null: false
    t.integer "posts_count", default: 0
    t.datetime "activated_at", precision: nil
    t.datetime "deleted_at", precision: nil
    t.datetime "banned_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "cards", "decks"
  add_foreign_key "chapters", "courses"
  add_foreign_key "class_rooms", "users"
  add_foreign_key "courses", "users"
  add_foreign_key "decks", "users"
  add_foreign_key "documents", "users"
  add_foreign_key "paragraphs", "documents"
  add_foreign_key "permissions", "roles"
  add_foreign_key "posts", "categories"
  add_foreign_key "posts", "documents"
  add_foreign_key "posts", "users"
end
