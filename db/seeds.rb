# frozen_string_literal: true

if Rails.env.local?
  Dir[Rails.root.join('db', 'seeds', Rails.env, '*.rb')].each do |seed|
    load seed
  end
end
