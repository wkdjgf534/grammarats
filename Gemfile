# frozen_string_literal: true

source "https://rubygems.org"

ruby "3.3.4"

gem "aasm"
gem "bcrypt", "~> 3"
gem "bootsnap", require: false
gem "cssbundling-rails"
gem "dry-monads"
gem "dry-validation"
gem "email_validator"
gem "haml-rails"
gem "heroicon"
gem "image_processing", "~> 1"
gem "jsbundling-rails"
gem "pagy", "~> 5"
gem "pg", "~> 1"
gem "propshaft"
gem "puma", ">= 5.0"
gem "pundit", "~> 2.3"
gem "rails", "~> 7"
gem "rails-i18n", "~> 7"
gem "ransack"
gem "redis", ">= 4"
gem "stimulus-rails"
gem "turbo-rails"
gem "view_component"

group :development, :test do
  gem "debug", platforms: %i[mri windows]
  gem "factory_bot_rails"
  gem "faker"
  gem "haml_lint", require: false
  gem "rspec-rails", "~> 6"
  gem "rubocop-rails-omakase", require: false
  gem "sidekiq", "~> 7"
  gem "sidekiq-unique-jobs", "~> 8"
end

group :development do
  gem "brakeman"
  gem "bundler-audit", require: false
  gem "database_consistency", require: false
  gem "letter_opener"
  gem "rack-mini-profiler"
  gem "rubocop-performance", require: false
  gem "rubocop-rails", require: false
  gem "rubocop-rake", require: false
  gem "rubocop-rspec", require: false
  gem "ruby-lsp-rails"
  gem "solargraph", require: false
  gem "web-console"
end

group :test do
  gem "capybara"
  gem "database_cleaner-active_record"
  gem "rspec-sidekiq"
  gem "simplecov", require: false
end
