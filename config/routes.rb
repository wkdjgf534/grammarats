require "sidekiq/web"

Rails.application.routes.draw do
  # TODO auth security https://thoughtbot.com/blog/rails-advanced-routing-constraints
  mount Sidekiq::Web => "/sidekiq"

  namespace :admin do
    root "home#index"

    resources :categories, except: :show
    resources :courses, except: %i[new create]
    resources :roles, shallow: true do
      resources :permissions, only: %i[create update] do
        patch :toggle, on: :member # move to toggle controller
      end
    end

    resources :documents, shallow: true, except: %i[new create] do
      resources :paragraphs, except: %i[index show]
    end

    resources :posts, except: %i[new create]
    resources :decks, except: %i[new create]
  end

  namespace :dashboard do
    root "home#index"

    resources :documents do
      resources :paragraphs, shallow: true, except: %i[index show]
    end

    resources :courses do
      resources :chapters, shallow: true, except: %i[index show]
    end

    resources :decks do
      resources :cards, shallow: true, except: %i[index show]
    end

    resources :class_rooms
    resources :posts
  end

  resources :categories, only: %i[index show]
  resources :posts, only: %i[index show]

  resource :registration, only: %i[new create]
  resource :session, only: %i[new create destroy]
  resource :password, only: %i[edit update]
  resource :remind_password, only: %i[new create]
  resource :reset_password, only: %i[edit update]

  get "main", to: "pages#main"
  get "about_us", to: "pages#about_us"
  get "terms_of_service", to: "pages#terms_of_service"
  get "services", to: "pages#services"

  get "up", to: "rails/health#show", as: :rails_health_check

  root "pages#main"
end
