# frozen_string_literal: true

class RegistrationsController < ApplicationController
  def new
    @registration_form = User::RegistrationForm.new
  end

  def create
    @registration_form = User::RegistrationForm.new(user_registration_form_params)

    if (user = @registration_form.save)
      user_sign_in(user)
      redirect_to(root_path)
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def user_registration_form_params
    params.require(:user_registration_form).permit(
      :email,
      :password,
      :password_confirmation,
      :terms_of_service
    )
  end
end
