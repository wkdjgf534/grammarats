# frozen_string_literal: true

class PagesController < ApplicationController
  def main; end

  def about_us; end

  def terms_of_service; end

  def services; end
end
