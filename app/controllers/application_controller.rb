# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include AuthenticationConcern

  add_flash_types :success

  def model_name = controller_name.classify
end
