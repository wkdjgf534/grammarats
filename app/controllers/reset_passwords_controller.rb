# frozen_string_literal: true

class ResetPasswordsController < ApplicationController
  def edit
    @reset_password_form = User::ResetPasswordForm.new
  end

  def update
    @reset_password_form = User::ResetPasswordForm.new(reset_password_params.merge(token: token_params))

    if @reset_password_form.valid?
      user = @reset_password_form.user
      user.update(reset_password_params)
      redirect_to(new_session_path)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def token_params
    params.require(:token)
  end

  def reset_password_params
    params.require(:user_reset_password_form).permit(:password, :password_confirmation)
  end
end
