# frozen_string_literal: true

module Dashboard
  class ClassRoomsController < Dashboard::ApplicationController
    def index
      @query = ClassRoom.ransack(params[:query])
      @class_rooms = @query.result(distinct: true)
    end

    def show; end

    def new
      @class_room = ClassRoom.new
    end

    def edit; end

    def create
      @class_room = current_user.class_rooms.build(class_room_params)

      if @class_room.save
        redirect_to(dashboard_class_rooms_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @class_room.update(class_room_params)
        redirect_to(dashboard_class_rooms_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @class_room.destroy
      redirect_to(dashboard_class_rooms_path,
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_document
      @class_room = ClassRoom.find(params[:id])
    end

    def class_room_params
      params.require(:class_room).permit(:description, :title, :is_private)
    end
  end
end
