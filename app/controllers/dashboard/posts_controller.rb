# frozen_string_literal: true

module Dashboard
  class PostsController < Dashboard::ApplicationController
    before_action :load_post, only: %i[show edit update destroy]
    before_action :load_category, only: %i[new create edit update]
    before_action :load_document, only: %i[new create edit update]

    def index
      @query = Post.ransack(params[:query])
      @posts = @query.result(distinct: true).includes(:category, :document)
    end

    def show; end

    def new
      @post = Post.new
    end

    def edit; end

    def create
      @post = current_user.posts.build(post_params)

      if @post.save
        redirect_to(dashboard_posts_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @post.update(post_params)
        redirect_to(dashboard_posts_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @post.destroy
      redirect_to(dashboard_posts_path,
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_post
      @post = Post.find(params[:id])
    end

    def load_category
      @categories = Category
                    .activated
                    &.pluck(:title, :id)
    end

    def load_document
      @documents = Document
                   .filter_by_author(current_user)
                   .filter_by_kind(:post)
                   &.pluck(:title, :id)
    end

    def post_params
      params.require(:post).permit(:title, :category_id, :document_id)
    end
  end
end
