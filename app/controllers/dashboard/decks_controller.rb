# frozen_string_literal: true

module Dashboard
  class DecksController < Dashboard::ApplicationController
    before_action :load_deck, only: %i[show edit update destroy]

    def index
      @query = Deck.ransack(params[:query])
      @decks = @query.result(distinct: true)
    end

    def show; end

    def new
      @deck = Deck.new
    end

    def edit; end

    def create
      @deck = current_user.decks.build(deck_params)

      if @deck.save
        redirect_to(dashboard_decks_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @deck.update(deck_params)
        redirect_to(dashboard_decks_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      delete_service = ::Decks::DeleteService.call(deck: @deck)

      if delete_service.success?
        redirect_to(dashboard_decks_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(dashboard_decks_path, alert: delete_service.failure)
      end
    end

    private

    def load_deck
      @deck = Deck.find(params[:id])
    end

    def deck_params
      params.require(:deck).permit(:description, :title)
    end
  end
end
