# frozen_string_literal: true

module Dashboard
  class DocumentsController < Dashboard::ApplicationController
    before_action :load_document, only: %i[show edit update destroy]
    before_action :load_document_kinds, only: %i[new create edit update]

    def index
      @query = Document.filter_by_author(current_user).ransack(params[:query])
      @documents = @query.result(distinct: true).includes(:paragraphs)
    end

    def show; end

    def new
      @document = Document.new
    end

    def edit; end

    def create
      @document = current_user.documents.build(document_params)

      if @document.save
        redirect_to(dashboard_documents_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @document.update(document_params)
        redirect_to(dashboard_documents_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @document.destroy
      redirect_to(dashboard_documents_path,
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    # TODO: add duplicate action to allow user to copy existed document content into a new document
    # Maybe it is a good way create class duplicator with action new for (document and paragraph)

    private

    def load_document
      @document = Document.find(params[:id])
    end

    def load_document_kinds
      @kinds = ::DropdownAggregator.values(enum: Document.kinds)
    end

    def document_params
      params.require(:document).permit(:description, :title, :kind)
    end
  end
end
