# frozen_string_literal: true

module Dashboard
  class CardsController < Dashboard::ApplicationController
    before_action :load_deck, only: %i[new create]
    before_action :load_card, only: %i[edit update destroy]

    def new
      @card = @deck.cards.new
    end

    def edit; end

    def create
      # card_params = chapter_params.merge('position' => @deck.cards_count + 1)
      @card = @deck.cards.build(card_params)

      if @card.save
        redirect_to(dashboard_deck_path(@deck),
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @card.update(card_params)
        redirect_to(dashboard_deck_path(@card.deck_id),
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @card.destroy
      redirect_to(dashboard_deck_path(@card.deck_id),
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_deck
      @deck = Deck.find(params[:deck_id])
    end

    def load_card
      @card = Card.find(params[:id])
    end

    def card_params
      params.require(:card).permit(:title, :deck_id, :position)
    end
  end
end
