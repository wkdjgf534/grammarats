# frozen_string_literal: true

module Dashboard
  class ParagraphsController < Dashboard::ApplicationController
    before_action :load_document, only: %i[new create]
    before_action :load_paragraph, only: %i[edit update destroy]

    def new
      @paragraph = @document.paragraphs.new
    end

    def edit; end

    def create
      @paragraph = @document.paragraphs.build(paragraph_params)

      if @paragraph.save
        redirect_to(dashboard_document_path(@document),
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @paragraph.update(paragraph_params)
        redirect_to(dashboard_document_path(@paragraph.document_id),
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @paragraph.destroy
      redirect_to(dashboard_document_path(@paragraph.document_id),
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_document
      @document = Document.find(params[:document_id])
    end

    def load_paragraph
      @paragraph = Paragraph.find(params[:id])
    end

    def paragraph_params
      params.require(:paragraph).permit(:title, :content, :document_id, :position)
    end
  end
end
