# frozen_string_literal: true

module Dashboard
  class ChaptersController < Dashboard::ApplicationController
    before_action :load_course, only: %i[new create]
    before_action :load_chapter, only: %i[edit update destroy]

    def new
      @chapter = @course.chapters.new
    end

    def edit; end

    def create
      # chapter_params = chapter_params.merge('position' => @course.chapters_count + 1)
      @chapter = @course.chapters.build(chapter_params)

      if @chapter.save
        redirect_to(dashboard_course_path(@course),
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @chapter.update(chapter_params)
        redirect_to(dashboard_course_path(@chapter.course_id),
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @chapter.destroy
      redirect_to(dashboard_course_path(@chapter.course_id),
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_course
      @course = Course.find(params[:course_id])
    end

    def load_chapter
      @chapter = Chapter.find(params[:id])
    end

    def chapter_params
      params.require(:chapter).permit(:title, :course_id, :position)
    end
  end
end
