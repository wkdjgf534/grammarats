# frozen_string_literal: true

module Dashboard
  class ApplicationController < ApplicationController
    before_action :authenticate_user!
  end
end
