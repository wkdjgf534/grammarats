# frozen_string_literal: true

module Dashboard
  class CoursesController < Dashboard::ApplicationController
    before_action :load_course, only: %i[show edit update destroy]

    def index
      @query = Course.ransack(params[:query])
      @courses = @query.result(distinct: true)
    end

    def show; end

    def new
      @course = Course.new
    end

    def edit; end

    def create
      create_status = Courses::CreateService.call(course_params:, user: current_user)

      if create_status
        redirect_to(dashboard_courses_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @course.update(course_params)
        redirect_to(dashboard_courses_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      delete_service = ::Courses::DeleteService.call(course: @course)

      if delete_service.success?
        redirect_to(dashboard_courses_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(dashboard_courses_path, alert: delete_service.failure)
      end
    end

    private

    def load_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:description, :title)
    end
  end
end
