# frozen_string_literal: true

class PasswordsController < ApplicationController
  before_action :authenticate_user!

  def edit
    @password_form = User::PasswordForm.new
  end

  def update
    @password_form = User::PasswordForm.new(password_params)

    # TODO: Check that only current user can change password for him/her self
    if @password_form.valid?
      current_user.update(password_params)
      redirect_to root_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def password_params
    params.require(:user_password_form).permit(
      :password,
      :password_confirmation,
      :password_challenge
    )
  end
end
