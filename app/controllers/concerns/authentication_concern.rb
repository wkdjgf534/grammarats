# frozen_string_literal: true

# Module contains additional methods for authentication
module AuthenticationConcern
  extend ActiveSupport::Concern

  included do
    private

    def user_sign_in(user)
      Current.user = user
      reset_session
      session[:user_id] = user.id
    end

    def user_sign_out
      Current.user = Guest.new
      reset_session
    end

    def user_signed_in?
      !current_user.guest?
    end

    def current_user
      Current.user ||= User.find_by(id: session[:user_id]) || Guest.new
    end

    def authenticate_user!
      redirect_to root_path, alert: "You must be logged in before dinig that" unless user_signed_in?
    end

    helper_method :current_user, :user_signed_in?, :authenticate_user!
  end
end
