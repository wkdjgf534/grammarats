# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :load_post, only: :show

  def index
    @posts = Post.activated
  end

  def show; end

  private

  def load_post
    @post = Post.find(params[:id])
  end
end
