# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :load_category, only: :show

  def index
    @categories = Category.activated
  end

  def show; end

  private

  def load_category
    @category = Category.find(params[:id])
  end
end
