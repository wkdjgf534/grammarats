# frozen_string_literal: true

class SessionsController < ApplicationController
  before_action :authenticate_user!, only: :destroy

  def new
    @sign_in_form = User::SignInForm.new
  end

  def create
    @sign_in_form = User::SignInForm.new(user_sign_in_form_params)

    if (user = @sign_in_form.authenticate)
      user_sign_in(user)
      redirect_to root_path, success: t(".success", username: current_user.email)
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    user_sign_out
    redirect_to root_path, notice: t(".notice")
  end

  private

  def user_sign_in_form_params
    params.require(:user_sign_in_form).permit(:email, :password)
  end
end
