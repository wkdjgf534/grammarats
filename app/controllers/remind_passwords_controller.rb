# frozen_string_literal: true

class RemindPasswordsController < ApplicationController
  def new
    @remind_password_form = User::RemindPasswordForm.new
  end

  def create
    @remind_password_form = User::RemindPasswordForm.new(remind_password_params)

    if @remind_password_form.valid?
      user = @remind_password_form.user
      Users::RemindPasswordJob.perform_async(user.id)
      redirect_to(root_path)
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def remind_password_params
    params.require(:user_remind_password_form).permit(:email)
  end
end
