# frozen_string_literal: true

module Admin
  class RolesController < Admin::ApplicationController
    before_action :load_role, only: %i[show edit update destroy]

    def index
      @query = Role.ransack(params[:query])
      @roles = @query.result(distinct: true)
    end

    def show
      @permissions = @role.permission_data
    end

    def new
      @role = Role.new
    end

    def edit; end

    def create
      @role = Role.new(role_params)

      if @role.save
        redirect_to(admin_roles_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      if @role.update(role_params)
        redirect_to(admin_roles_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      delete_service = ::Roles::DeleteService.call(role: @role)

      if delete_service.success
        redirect_to(admin_roles_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(admin_roles_path, alert: delete_service.failure)
      end
    end

    private

    def load_role
      @role = Role.find(params[:id])
    end

    def role_params
      params.require(:role).permit(:name, :description)
    end
  end
end
