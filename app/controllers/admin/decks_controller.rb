# frozen_string_literal: true

module Admin
  class DecksController < Admin::ApplicationController
    before_action :load_deck, only: %i[show edit update destroy]

    def index
      @query = Deck.ransack(params[:query])
      @decks = @query.result(distinct: true)
    end

    def show; end

    def edit; end

    def update
      if @deck.update(deck_params)
        redirect_to(admin_decks_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      delete_service = ::Decks::DeleteService.call(deck: @deck)

      if delete_service.success?
        redirect_to(admin_decks_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(admin_decks_path, alert: delete_service.failure)
      end
    end

    private

    def load_deck
      @deck = Deck.find(params[:id])
    end

    def deck_params
      params.require(:deck).permit(:description, :title)
    end
  end
end
