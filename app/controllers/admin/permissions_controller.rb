# frozen_string_literal: true

module Admin
  class PermissionsController < Admin::ApplicationController
    before_action :load_role, only: %i[create update]
    before_action :load_permission, only: %i[toggle]

    def create
      create_service = ::Permissions::CreateService.call(role: @role)

      # if create_service.success?
      #  redirect_to(admin_role_path(@role),
      #              status: :see_other,
      #              notice: t('common.destroy.notice', model_name:))
      # else
      #  redirect_to(admin_role_path(@role), alert: create_service.failure)
      # end
    end

    def update
      update_service = ::Permissions::UpdateService.call(role: @role)

      if update_service.success?
        redirect_to(admin_role_path(@role),
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(admin_role_path(@role), alert: create_service.failure)
      end
    end

    #
    # #https://onrails.blog/2024/03/06/stimulusjs-tutorial-update-model-with-checkbox-using-turbo-morphing/
    # Todo: move to activate controller create(activate), destroy(deactivate)
    def toggle = @permission.update(is_active: !@permission.is_active?)

    private

    def load_permission = @permission = Permission.find(params[:id])

    def permission_params
      params.require(:permission).permit(:is_active)
    end
  end
end
