# frozen_string_literal: true

module Admin
  class CoursesController < Admin::ApplicationController
    before_action :load_course, only: %i[show edit update destroy]

    def index
      @query = Course.ransack(params[:query])
      @courses = @query.result(distinct: true)
    end

    def show; end

    def edit; end

    def update
      if @course.update(course_params)
        redirect_to(admin_courses_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      delete_service = ::Courses::DeleteService.call(course: @course)

      if delete_service.success?
        redirect_to(admin_courses_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(admin_courses_path, alert: delete_service.failure)
      end
    end

    private

    def load_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:description, :title)
    end
  end
end
