# frozen_string_literal: true

module Admin
  class DocumentsController < Admin::ApplicationController
    before_action :load_document, only: %i[show edit update destroy]
    before_action :load_document_kinds, only: %i[edit update]

    def index
      @query = Document.ransack(params[:query])
      @documents = @query.result(distinct: true).includes(:paragraphs)
    end

    def show; end

    def edit; end

    def update
      if @document.update(document_params)
        redirect_to(admin_documents_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      @document.destroy
      redirect_to(admin_documents_path,
                  status: :see_other,
                  notice: t("common.destroy.notice", model_name:))
    end

    private

    def load_document
      @document = Document.find(params[:id])
    end

    def load_document_kinds
      @kinds = ::DropdownAggregator.values(enum: Document.kinds)
    end

    def document_params
      params.require(:document).permit(:description, :title, :kind)
    end
  end
end
