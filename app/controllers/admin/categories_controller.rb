# frozen_string_literal: true

module Admin
  class CategoriesController < Admin::ApplicationController
    before_action :load_category, only: %i[edit update destroy]

    def index
      @query = Category.ransack(params[:query])
      @categories = @query.result(distinct: true)
    end

    def new
      @category = Category.new
    end

    def edit; end

    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to(admin_categories_path,
                    success: t("common.create.success", model_name:))
      else
        render :new, status: :unprocessable_entity
      end
    end

    def update
      update_service = ::Categories::UpdateService.call(category: @category, category_params:)

      if update_service.success?
        redirect_to(admin_categories_path,
                    success: t("common.update.success", model_name:))
      else
        render :edit, status: :unprocessable_entity
        # flash update_service.failure
      end
    end

    def destroy
      delete_service = ::Categories::DeleteService.call(category: @category)

      if delete_service.success?
        redirect_to(admin_categories_path,
                    status: :see_other,
                    notice: t("common.destroy.notice", model_name:))
      else
        redirect_to(admin_categories_path, alert: delete_service.failure)
      end
    end

    private

    def load_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:title, :description)
    end
  end
end
