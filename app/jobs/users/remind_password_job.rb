# frozen_string_literal: true

require "sidekiq"

module Users
  class RemindPasswordJob
    include Sidekiq::Job

    sidekiq_options retry: 5, queue: "critical"

    def perform(user_id)
      # TODO: Move to service
      user = User.find(user_id)
      token = user.generate_token_for(:password_reset)
      UserMailer.reset_password(user:, token:).deliver_later
      RailsLogger.log("sent an reset email to the user with id: #{user_id}, email: {user.email}")
    end
  end
end
