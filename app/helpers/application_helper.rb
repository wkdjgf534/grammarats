# frozen_string_literal: true

module ApplicationHelper
  def application_title = Rails.application.class.module_parent.to_s

  def full_title(page_title = "")
    page_title.present? ? "#{application_title} | #{page_title}" : application_title
  end
end
