# frozen_string_literal: true

# Category additional methods
module CategoryRepository
  extend ActiveSupport::Concern

  included do
    scope :archived,  -> { where(state: "archived") }
    scope :activated, -> { where(state: "activated") }
  end
end
