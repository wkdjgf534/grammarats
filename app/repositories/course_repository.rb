# frozen_string_literal: true

# Course additional methods
module CourseRepository
  extend ActiveSupport::Concern

  included do
    scope :pending,   -> { where(state: "pending") }
    scope :archived,  -> { where(state: "archived") }
    scope :activated, -> { where(state: "activated") }
  end
end
