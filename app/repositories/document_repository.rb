# frozen_string_literal: true

# Document additional methods
module DocumentRepository
  extend ActiveSupport::Concern

  included do
    scope :filter_by_author, ->(user) { where(user:) }
    scope :filter_by_kind, ->(kind) { where(kind:) }
  end
end
