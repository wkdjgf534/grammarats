# frozen_string_literal: true

# Document additional methods
module ClassRoomRepository
  extend ActiveSupport::Concern

  included do
    scope :get_private, -> { where(is_private: true) }
    scope :get_public, -> { where(is_private: false) }
    scope :by_teacher, ->(user) { where(user:) }
  end
end
