# frozen_string_literal: true

# Deck additional methods
module DeckRepository
  extend ActiveSupport::Concern

  included do
    scope :pending,   -> { where(state: "pending") }
    scope :archived,  -> { where(state: "archived") }
    scope :activated, -> { where(state: "activated") }
  end
end
