# frozen_string_literal: true

# Post additional methods
module PostRepository
  extend ActiveSupport::Concern

  included do
    scope :filter_by_author, ->(user) { where(user:) }
    scope :pending,   -> { where(state: "pending") }
    scope :archived,  -> { where(state: "archived") }
    scope :activated, -> { where(state: "activated") }
  end
end
