# frozen_string_literal: true

# User additional methods
module UserRepository
  extend ActiveSupport::Concern

  included do
    scope :banned,    -> { where(state: "banned") }
    scope :deleted,   -> { where(state: "deleted") }
    scope :activated, -> { where(state: "activated") }
  end
end
