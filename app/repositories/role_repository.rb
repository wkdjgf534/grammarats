# frozen_string_literal: true

# Role additional methods
module RoleRepository
  extend ActiveSupport::Concern

  included do
    scope :pending,   -> { where(state: "pending") }
    scope :archived,  -> { where(state: "archived") }
    scope :activated, -> { where(state: "activated") }
  end

  # [
  #   {
  #     :entity => "Admin/Roles",
  #     :id => 1
  #     :verbs => ["index", "create", "new", "edit", "show", "update", "update", "destroy"],
  #     :statuses => [false, false, false, false, false, false, false, false]
  #   },
  #   {
  #     :entity => "Dashboard/Class Rooms",
  #     :id => 2
  #     :verbs => ["index", "create", "new", "edit", "show", "update", "update", "destroy"],
  #     :statuses => [false, false, false, false, false, false, false, false]
  #   },
  #   ...
  # ]
  def permission_data
    sql = <<~SQL.squish
      SELECT
        permissions.entity,
        json_agg(permissions.id) AS ids,
        json_agg(permissions.verb) AS verbs,
        json_agg(permissions.is_active) AS statuses
      FROM permissions
      INNER JOIN roles ON roles.id = permissions.role_id
      GROUP BY entity
    SQL

    rows = ActiveRecord::Base.connection.select_all(sql)
    rows.map do |row|
      {
        model: row["entity"],
        id: JSON.parse(row["ids"]),
        actions: JSON.parse(row["verbs"]),
        statuses: JSON.parse(row["statuses"])
      }
    end
  end
end
