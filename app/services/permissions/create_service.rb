# frozen_string_literal: true

module Permissions
  class CreateService < ApplicationService
    attr_reader :params, :permission_data

    def initialize(params:, permission_data: PermittedEntity.controllers_with_actions)
      super
      @params = params
      @permission_data = permission_data
    end

    def call(role_validator: Roles::Validator, permission_mutator: PermissionMutator)
      yield role_validator.call(params)
      ActiveRecord::Base.transaction { permissions(permission_mutator) }
      Success()
    end

    private

    def permissions(permission_mutator)
      permission_data.each do |elem|
        entity = elem[:entity]
        verbs = elem[:verbs]
        verbs.each do |verb|
          permission_mutator.create!(role:, entity:, verb:)
        end
      end
    end
  end
end
