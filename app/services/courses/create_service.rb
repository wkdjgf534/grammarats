# frozen_string_literal: true

module Courses
  class CreateService < ApplicationService
    attr_reader :params

    INITIAL_CHAPTER_NAME = "New chapter".freeze

    def initialize(params)
      super
      @params = params
    end

    def call
      ActiveRecord::Base.transaction do
        course = CourseMutator.create!(params)
        ChapterMutator.create!({ title: INITIAL_CHAPTER_NAME, course: })
      end
      Success() # check it first
    rescue ActiveRecord::Rollback => e
      # TODO: extend error message
      Rails.logger.error(e.message)
      Failure()
    end
  end
end
