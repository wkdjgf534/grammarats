# frozen_string_literal: true

module Courses
  class UpdateService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    def call(course_validator: Validator, course_mutator: CourseMutator)
      yield course_validator.call(params)
      course_mutator.update!(params)
      Success()
    end
  end
end
