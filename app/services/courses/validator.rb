# frozen_string_literal: true

module Courses
  module Validator
    extend Dry::Monads[:result, :do]

    ERROR_MESSAGES = {
      object_is_nil: I18n.t("errors.object_is_nil"),
      object_is_active: I18n.t("errors.object_is_active")
    }.freeze

    def self.call(params)
      course = params[:course]
      error_message =
        if course.nil?
          ERROR_MESSAGES[:object_is_nil]
        elsif course.activated?
          ERROR_MESSAGES[:object_is_active]
        end

      error_message.nil? ? Success() : Failure(error_message)
    end
  end
end
