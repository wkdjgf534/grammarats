# frozen_string_literal: true

class ApplicationService
  include Dry::Monads[:result, :do]

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def self.call(params) = new(params).call
end
