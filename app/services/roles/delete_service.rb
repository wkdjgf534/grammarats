# frozen_string_literal: true

module Roles
  class DeleteService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    def call(role_validator: Roles::Validator, role_mutator: RoleMutator)
      yield role_validator.call(params)
      role_mutator.destroy!(params)
      Success()
    end
  end
end
