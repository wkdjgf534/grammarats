# frozen_string_literal: true

module Roles
  class Validator
    extend Dry::Monads[:result, :do]

    ERROR_MESSAGES = {
      object_is_nil: I18n.t("errors.object_is_nil"),
      object_is_active: I18n.t("errors.object_is_active")
    }.freeze

    def self.call(params)
      role = params[:role]
      error_message =
        if role.nil?
          ERROR_MESSAGES[:object_is_nil]
        elsif role.activated?
          ERROR_MESSAGES[:object_is_active]
        end

      error_message.nil? ? Success() : Failure(error_message)
    end
  end
end
