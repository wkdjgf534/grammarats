# frozen_string_literal: true

module Decks
  module Validator
    extend Dry::Monads[:result, :do]

    ERROR_MESSAGES = {
      object_is_nil: I18n.t("errors.object_is_nil"),
      object_is_active: I18n.t("errors.object_is_active")
    }.freeze

    def self.call(params)
      deck = params[:deck]
      error_message =
        if deck.nil?
          ERROR_MESSAGES[:object_is_nil]
        elsif deck.activated?
          ERROR_MESSAGES[:object_is_active]
        end

      error_message.nil? ? Success() : Failure(error_message)
    end
  end
end
