# frozen_string_literal: true

module Decks
  class DeleteService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    def call(deck_validator: Validator, deck_mutator: DeckMutator)
      yield deck_validator.call(params)
      deck_mutator.destroy!(params)
      Success()
    end
  end
end
