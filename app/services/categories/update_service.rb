# frozen_string_literal: true

module Categories
  class UpdateService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    def call(category_validator: Validator, category_mutator: CategoryMutator)
      yield category_validator.call(params)
      category_mutator.update!(params)
      Success()
    end
  end
end
