# frozen_string_literal: true

module Categories
  class DeleteService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    # TODO: dry-rb dependency injection via containers
    def call(category_validator: Validator, category_mutator: CategoryMutator)
      yield category_validator.call(params)
      category_mutator.destroy!(params)
      Success()
    end
  end
end
