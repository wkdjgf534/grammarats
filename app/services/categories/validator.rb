# frozen_string_literal: true

module Categories
  class Validator
    extend Dry::Monads[:result, :do]

    ERROR_MESSAGES = {
      object_is_nil: I18n.t("errors.object_is_nil"),
      object_is_active: I18n.t("errors.object_is_active"),
      object_contains_dependent_records: I18n.t("errors.object_contains_dependent_records")
    }.freeze

    def self.call(params)
      category = params[:category]
      error_message =
        if category.nil?
          ERROR_MESSAGES[:object_is_nil]
        elsif category.activated?
          ERROR_MESSAGES[:object_is_active]
        elsif category.posts_count.positive?
          ERROR_MESSAGES[:object_contains_dependent_records]
        end

      error_message.nil? ? Success() : Failure(error_message)
    end
  end
end
