# frozen_string_literal: true

module Chapters
  class DeleteService < ApplicationService
    attr_reader :params

    def initialize(params)
      super
      @params = params
    end

    def call(course_validator: Validator, chapter_mutator: ChapterMutator)
      yield course_validator.call(params)
      chapter_mutator.destroy!(params)
      Success()
    end
  end
end
