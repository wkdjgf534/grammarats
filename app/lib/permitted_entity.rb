# frozen_string_literal: true

class PermittedEntity
  PERMITTED_NAMESPACES = %w[admin dashboard].freeze

  class << self
    # [
    #   {
    #     :entity=>"Admin/Roles",
    #     :verbs=>["index", "create", "new", "edit", "show", "update", "update", "destroy"],
    #   },
    #   {
    #     :entity=>"Dashboard/Class Rooms",
    #     :verbs=>["index", "create", "new", "edit", "show", "update", "update", "destroy"],
    #   },
    #   ...
    # ]
    def controllers_with_actions
      filtered_controllers = routes.map do |route|
        route.defaults if route.defaults[:controller]&.start_with?(*PERMITTED_NAMESPACES)
      end
      group_controllers(filtered_controllers)
    end

    private

    def routes = Rails.application.routes.set.anchored_routes

    def group_controllers(filtered_controllers)
      filtered_controllers
        .compact_blank
        .group_by { |route| route[:controller] }
        .map { |k, v| { entity: k.titleize, verbs: v.map { |route| route[:action] } } }
    end
  end
end
