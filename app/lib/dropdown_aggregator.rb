# frozen_string_literal: true

class DropdownAggregator
  def self.values(enum:) = enum.keys.map { |key| [ key.humanize, key ] }
end
