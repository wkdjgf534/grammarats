# frozen_string_literal: true

class User
  class SignInForm
    include ActiveModel::Model

    attr_accessor :email, :password

    validates :email, presence: true, email: true
    validates :password, presence: true

    def authenticate
      return false unless valid?

      User.authenticate_by(email:, password:)
    end
  end
end
