# frozen_string_literal: true

class User
  class RegistrationForm
    include ActiveModel::Model

    attr_accessor :email, :password, :password_confirmation, :terms_of_service

    validates :email, presence: true, email: true
    validates :terms_of_service, acceptance: { accept: "1" }
    validates :password, confirmation: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }
    validates :password_confirmation, presence: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }
    validate  :email_unique?

    def save
      return false unless valid?

      User.create(email:, password:, password_confirmation:)
    end

    private

    def email_unique?
      return true if User.find_by(email:).blank?

      errors.add(:email, "is taken") # TODO: move message to locale
    end
  end
end
