# frozen_string_literal: true

class User
  class ResetPasswordForm
    include ActiveModel::Model

    attr_accessor :email, :password, :password_confirmation, :token

    validates :password, confirmation: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }
    validates :password_confirmation, presence: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }

    def user = User.find_by_token_for(:password_reset, token)
  end
end
