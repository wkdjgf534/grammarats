# frozen_string_literal: true

class User
  class RemindPasswordForm
    include ActiveModel::Model

    attr_accessor :email

    validates :email, presence: true, email: true
    validate  :email_present?

    def user
      @user ||= User.find_by(email:)
    end

    private

    def email_present?
      return true if user.present?

      errors.add(:email, "does not exist") # TODO: move message to locale
    end
  end
end
