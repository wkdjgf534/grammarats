# frozen_string_literal: true

class User
  class PasswordForm
    include ActiveModel::Model

    attr_accessor :password, :password_confirmation, :password_challenge

    validates :password_challenge, presence: true
    validates :password, confirmation: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }
    validates :password_confirmation, presence: true, length: { in: PASSWORD[:min]..PASSWORD[:max] }
    validate  :new_password_unique?

    private

    def new_password_unique?
      return true unless password_challenge.in?([ password, password_confirmation ])

      errors.add(:password, "must not be like the previous password") # TODO
    end
  end
end
