# frozen_string_literal: true

class Role < ApplicationRecord
  include RoleRepository
  include RoleStateMachine

  NAME = { min: 2, max: 255 }.freeze
  DESCRIPTION = { max: 16_384 }.freeze

  has_many :permissions, dependent: :destroy

  validates :name, presence: true,
                   length: { in: NAME[:min]..NAME[:max] },
                   uniqueness: { case_sensitive: false }

  validates :description, length: { maximum: DESCRIPTION[:max] }
  validates :state, presence: true, inclusion: { in: %w[pending archived activated] }

  normalizes :description, with: ->(description) { description.strip.capitalize }
  normalizes :name, with: ->(name) { name.strip.capitalize }

  def self.ransackable_attributes(_auth_object = nil)
    %w[activated_at archived_at created_at description name]
  end
end
