# frozen_string_literal: true

require "securerandom"

class User < ApplicationRecord
  include UserRepository
  include UserStateMachine

  NAME = { max: 200 }.freeze
  PASSWORD = { min: 8, max: 70 }.freeze

  has_secure_password

  has_many :documents
  has_many :class_rooms
  has_many :courses
  has_many :posts
  has_many :decks

  validates :state, presence: true, inclusion: { in: %w[activated banned deleted] }

  normalizes :email, with: ->(email) { email.downcase.strip }
  normalizes :first_name, :last_name, with: ->(attribute) { attribute.capitalize.strip }

  generates_token_for :password_reset, expires_in: 15.minutes do
    password_salt&.last(10)
  end

  # generates_token_for :email_confirmation, expires_in: 24.hours do
  #  email
  # end

  def guest? = false

  def full_name
    # return 'Deleted user' if self.deleted?
    # return 'Anonymous' if self.private_name?

    [ first_name, last_name ].compact.join(" ").presence || "Anonymous"
  end
end
