# frozen_string_literal: true

class Post < ApplicationRecord
  include PostRepository
  include PostStateMachine

  TITLE = { min: 4, max: 255 }.freeze
  SLUG = { min: 4, max: 300 }.freeze

  belongs_to :user, counter_cache: true
  belongs_to :category, counter_cache: true
  belongs_to :document

  before_validation :set_slug

  validates :title, presence: true,
                    length: { in: TITLE[:min]..TITLE[:max] }

  validates :state, presence: true, inclusion: { in: %w[pending activated archived] }
  validates :slug, presence: true, length: { in: SLUG[:min]..SLUG[:max] }

  normalizes :slug, with: ->(slug) { slug.strip.parameterize }
  normalizes :title, with: ->(title) { title.strip.capitalize }

  delegate :title, to: :category, prefix: :category
  delegate :title, to: :category, prefix: :document
  delegate :email, to: :user, prefix: :author

  def self.ransackable_attributes(_auth_object = nil)
    %w[category_id document_id id published_at slug state title user_id]
  end

  private

  def set_slug = self.slug = title&.strip&.parameterize
end
