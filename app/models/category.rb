# frozen_string_literal: true

class Category < ApplicationRecord
  include CategoryRepository
  include CategoryStateMachine

  TITLE = { min: 2, max: 255 }.freeze
  DESCRIPTION = { max: 16_384 }.freeze
  SLUG = { min: 2, max: 300 }.freeze

  has_many :posts

  before_validation :set_slug

  validates :title, presence: true,
                    length: { in: TITLE[:min]..TITLE[:max] },
                    uniqueness: { case_sensitive: false }

  validates :description, length: { maximum: DESCRIPTION[:max] }
  validates :state, presence: true, inclusion: { in: %w[activated archived] }
  validates :slug, presence: true, length: { in: SLUG[:min]..SLUG[:max] }

  normalizes :description, with: ->(description) { description.strip.capitalize }
  normalizes :slug, with: ->(slug) { slug.strip.parameterize }
  normalizes :title, with: ->(title) { title.strip.capitalize }

  attribute :posts_count, default: 0

  def self.ransackable_attributes(_auth_object = nil)
    %w[archived_at created_at posts_count slug state title]
  end

  def self.ransackable_associations(_auth_object = nil) = [ "posts" ]

  private

  def set_slug
    self.slug = title&.strip&.parameterize
  end
end
