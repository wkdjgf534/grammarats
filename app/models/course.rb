# frozen_string_literal: true

class Course < ApplicationRecord
  include CourseRepository
  include CourseStateMachine

  TITLE = { min: 2, max: 255 }.freeze
  DESCRIPTION = { max: 16_384 }.freeze

  belongs_to :user
  has_many :chapters

  validates :title, presence: true,
                    length: { in: TITLE[:min]..TITLE[:max] }

  validates :description, length: { maximum: DESCRIPTION[:max] }
  validates :state, presence: true, inclusion: { in: %w[pending archived activated] }

  normalizes :description, with: ->(description) { description.strip.capitalize }
  normalizes :title, with: ->(title) { title.strip.capitalize }

  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at description state title updated_at user_id]
  end

  def self.ransackable_associations(_auth_object = nil) = [ "user" ]
end
