# frozen_string_literal: true

class ClassRoom < ApplicationRecord
  include ClassRoomRepository

  TITLE = { min: 2, max: 255 }.freeze
  DESCRIPTION = { max: 16_384 }.freeze

  belongs_to :user

  validates :title, presence: true,
                    length: { in: TITLE[:min]..TITLE[:max] }

  validates :description, length: { maximum: DESCRIPTION[:max] }

  normalizes :title, with: ->(title) { title.strip.capitalize }
  normalizes :description, with: ->(description) { description.strip.capitalize }

  attribute :is_private, default: false

  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at description is_private title updated_at user_id]
  end

  def self.ransackable_associations(_auth_object = nil) = [ "user" ]
end
