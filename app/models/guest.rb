# frozen_string_literal: true

require "securerandom"

class Guest
  def id = SecureRandom.uuid

  def email; end

  def guest? = true

  def full_name = "Guest user"
end
