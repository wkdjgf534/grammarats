# frozen_string_literal: true

class Document < ApplicationRecord
  include DocumentRepository

  TITLE = { min: 2, max: 255 }.freeze
  DESCRIPTION = { max: 16_384 }.freeze

  belongs_to :user
  has_many :paragraphs, dependent: :destroy
  # TODO: check it, need additional functionality
  has_one :post

  validates :title, presence: true,
                    length: { in: TITLE[:min]..TITLE[:max] }

  validates :description, length: { maximum: DESCRIPTION[:max] }

  normalizes :description, with: ->(description) { description.strip.capitalize }
  normalizes :title, with: ->(title) { title.strip.capitalize }

  attribute :paragraphs_count, default: 0
  delegate :email, to: :user, prefix: :author

  enum kind: { lesson: 0, post: 1, legal_agreement: 2, privacy_policy: 3, terms_of_service: 4 }, _default: 0

  def self.ransackable_attributes(_auth_object = nil)
    %w[created_at description kind paragraphs_count title user_id]
  end

  def humanized_kind = kind.humanize
end
