# frozen_string_literal: true

# Deck State Machine
# This concern contains all nessesary information for deck state machine (AASM, methods)
module DeckStateMachine
  extend ActiveSupport::Concern

  included do
    include AASM

    aasm :state, timestamps: true do
      state :pending, initial: true
      state :activated
      state :archived

      event :mark_as_pending do
        transitions to: :pending

        after do
          unset_archived_time
          unset_activated_time
        end
      end

      event :mark_as_archived do
        transitions to: :archived
      end

      event :mark_as_activated do
        transitions to: :activated
      end
    end

    private

    def unset_archived_time = self.archived_at = nil

    def unset_activated_time = self.activated_at = nil
  end
end
