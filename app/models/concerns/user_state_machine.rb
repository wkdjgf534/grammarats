# frozen_string_literal: true

# User State Machine
# This concern contains all nessesary information for user state machine (AASM, methods)
module UserStateMachine
  extend ActiveSupport::Concern

  included do
    include AASM

    aasm :state, timestamps: true do
      # state :pending, initial: true
      state :activated, initial: true
      state :banned
      state :deleted

      event :mark_as_activated do
        transitions to: :activated

        after do
          unset_banned_time
          unset_deleted_time
        end
      end

      event :mark_as_banned do
        transitions to: :banned
      end

      # TODO: can back to pending state only
      event :mark_as_deleted do
        transitions to: :deleted
      end
    end

    private

    def unset_banned_time = self.banned_at = nil

    def unset_deleted_time = self.deleted_at = nil
  end
end
