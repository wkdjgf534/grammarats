# frozen_string_literal: true

# Category State Machine
# This concern contains all nessesary information for category state machine (AASM, methods)
module CategoryStateMachine
  extend ActiveSupport::Concern

  included do
    include AASM

    aasm :state, timestamps: true do
      state :activated, initial: true
      state :archived

      event :mark_as_activated do
        transitions to: :activated, after: :unset_archived_time
      end

      event :mark_as_archived do
        transitions to: :archived
      end
    end

    private

    def unset_archived_time = self.archived_at = nil
  end
end
