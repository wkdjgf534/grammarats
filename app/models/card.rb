# frozen_string_literal: true

class Card < ApplicationRecord
  TITLE = { max: 255 }.freeze

  belongs_to :deck

  validates :title, length: { maximum: TITLE[:max] }
  validates :position, presence: true

  normalizes :title, with: ->(title) { title.strip.capitalize }

  attribute :position, default: 0
end
