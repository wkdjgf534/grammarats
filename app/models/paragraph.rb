# frozen_string_literal: true

class Paragraph < ApplicationRecord
  TITLE = { max: 255 }.freeze

  belongs_to :document, counter_cache: true

  validates :title, length: { maximum: TITLE[:max] }
  validates :content, presence: true
  validates :position, presence: true

  normalizes :title, with: ->(title) { title.strip.capitalize }

  attribute :position, default: 0
end
