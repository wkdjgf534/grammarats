# frozen_string_literal: true

class Chapter < ApplicationRecord
  TITLE = { max: 255 }.freeze

  belongs_to :course, counter_cache: true

  validates :title, length: { maximum: TITLE[:max] }
  validates :position, presence: true

  normalizes :title, with: ->(title) { title.strip.capitalize }

  attribute :position, default: 0
end
