# frozen_string_literal: true

class Permission < ApplicationRecord
  belongs_to :role

  attribute :is_activate, default: false
end
