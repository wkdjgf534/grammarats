# frozen_string_literal: true

class CategoryMutator
  def self.update!(params)
    category = params[:category]
    category_params = params[:category_params]
    category.update!(category_params)
  end

  def self.destroy!(params)
    category = params[:category]
    category.destroy!
  end
end
