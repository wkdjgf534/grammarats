# frozen_string_literal: true

class DeckMutator
  def self.create!(params)
    user = params[:user]
    deck_params = params[:deck_params]
    Deck.create!(deck_params.merge(user:))
  end

  def self.update!(params)
    deck = params[:deck]
    deck_params = params[:deck_params]
    deck.update!(deck_params)
  end

  def self.destroy!(params)
    deck = params[:deck]
    deck.destroy!
  end
end
