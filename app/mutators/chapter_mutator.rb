# frozen_string_literal: true

class ChapterMutator
  def self.create!(params) = Chapter.create!(params)
end
