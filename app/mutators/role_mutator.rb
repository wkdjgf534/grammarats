# frozen_string_literal: true

class RoleMutator
  def self.destroy!(params)
    role = params[:role]
    role.destroy!
  end
end
