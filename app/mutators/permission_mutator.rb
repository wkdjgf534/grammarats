# frozen_string_literal: true

class PermissionMutator
  def self.create!(params) = Permission.create!(params)
end
