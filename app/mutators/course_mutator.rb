# frozen_string_literal: true

class CourseMutator
  def self.create!(params)
    user = params[:user]
    course_params = params[:course_params]
    Course.create!(course_params.merge(user:))
  end

  def self.update!(params)
    course = params[:course]
    course_params = params[:course_params]
    course.update!(course_params)
  end

  def self.destroy!(params)
    course = params[:course]
    course.destroy!
  end
end
