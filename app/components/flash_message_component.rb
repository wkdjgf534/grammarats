# frozen_string_literal: true

class FlashMessageComponent < ViewComponent::Base
  def initialize(type:, message:)
    @type = type
    @message = message
  end

  # https://reinteractive.com/articles/how-to-create-flash-messages-in-Rails-7
end
