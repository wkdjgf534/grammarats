# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def reset_password(user:, token:)
    @token = token
    mail to: user.email, subject: "Reset password"
  end
end
